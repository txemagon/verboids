#ifndef __TARGETABLE_H__
#define __TARGETABLE_H__

class Targetable
{
    public:
	virtual double x () const = 0;
	virtual double y () const = 0;
};

#endif
