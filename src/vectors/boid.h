#ifndef __BOID_H__
#define __BOID_H__

#include "targetable.h"
#include "vec2d.h"

class Boid;
typedef Vec2d (Boid::*BehaviourFn)(Boid * const);


class Boid: public Targetable
{

    public:
        /******** PUBLIC CONSTANTS ************/
        enum Behaviour {SEEK, FLEE, TOTAL_B};

    private:
        Vec2d pos, vel;
        static const double VMAX;
        static const double AMAX;
        static BehaviourFn behaviour_fn[TOTAL_B+1];



	Targetable *target;
        Behaviour active_behaviour;

        Vec2d parse_target ();
    public:

        /********** Catalogable Methods ***********/
        Vec2d seek (Boid * const self);         /* Note the use of self instead of this */
        Vec2d flee (Boid * const self);         /* Note also the first parameter in the call is this. */

        /********* CONSTRUCTORS *********/
	Boid (double x=0, double y=0, double vx=0, double vy=0);
	Boid (Vec2d pos, Vec2d vel = Vec2d(0,0));

        /******** TARGETABLE REQUIREMENTS ************/
        double x () const;
        double y () const;

        /************ GETTER & SETTERS **************/
        void set_pos ( Vec2d pos );
        void set_pos ( double x, double y );
        Vec2d get_pos ();

	void set_vel ( Vec2d vel );
        void set_vel ( double x, double y );
        Vec2d get_vel ();

	void set_target ( Targetable *target );
        void set_target ( double x, double y );
        Vec2d get_target ();


        void activate (Behaviour b);
        Behaviour get_active_behaviour ();

        /*************** IA LAYER ********************/

        Vec2d desired_accel();
};

#endif
