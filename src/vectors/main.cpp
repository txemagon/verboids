#include <iostream>
#include <stdlib.h>
#include "string"
#include <math.h>

#include "vec2d.h"
#include "boid.h"

#define MAX 2

using namespace std;

Vec2d
ask_coord (const char *label1, const char *label2)
{
    double x, y;

    system ("clear");
    cout << label1;
    cin >> x;
    cout << label2;
    cin >> y;

    Vec2d data(x, y);

    return data;
}

/* Una función que imprime el menú no debe hacer nada más. */
int
menu ()
{
  int option;

  system ("clear");
  cout << "1.- Buscar el objetivo" << endl;
  cout << "2.- Huir del objetivo" << endl;
  cout << "3.- Salir \n" << endl;
  cout << "Elige una opcion: ";
  cin >> option;

  return option - 1;
}

void
show_vector (const char *label, Vec2d v)
{
  cout << endl << "-> " << label << ": [" << v.x () <<"," << v.y() <<"]" << endl;
}

int
main (int argc, char *argv[])
{

 /* Data Input Block */
  Vec2d pos, vel, tar;
  Targetable *target;

  pos.set( ask_coord(
              "X [Boid]: ",
              "Y [Boid]: "
              ));

  vel.set ( ask_coord(
              "Vx [Boid]: ",
              "Vy [Boid]: "
          ));

  tar = ask_coord(
              "X [Target]: ",
              "Y [Target]: "
              );

  target = (Targetable *) new Vec2d(tar);
  int option = menu ();

  Boid manolito (pos, vel);
  manolito.set_target (target);

  manolito.activate( (Boid::Behaviour) option);
  Vec2d accel = manolito.desired_accel();


  /* Data Output Block */
  system ("clear");
  show_vector ("Position", pos);
  show_vector ("Velocity", vel);
  show_vector ("Target P", tar);
  show_vector ("Acceleration", accel);

  return EXIT_SUCCESS;
}
