#include <stdio.h>
#include "boid.h"

/* ALL BOIDS CHARACTERISTICS */
const double Boid::VMAX = 10;
const double Boid::AMAX = 2;

/********* Constructors *********/
Boid::Boid (double x, double y, double vx, double vy) :
	pos(x,y),
	vel(vx, vy)
{
	target = NULL;
}

Boid::Boid (Vec2d pos, Vec2d vel) :
	pos(pos),
	vel(vel)
{
	target = NULL;
}

/**************  Getter and Setters *************/

/* Targetable */
double Boid::x () const {return pos.x (); }
double Boid::y () const {return pos.y (); }

/* Position */
void Boid::set_pos (Vec2d pos)
{
	this->pos = pos;
}

void Boid::set_pos (double x, double y)
{
	pos = Vec2d (x,y);
}

Vec2d Boid::get_pos () { return pos;}

/* Velocity */
void Boid::set_vel (Vec2d vel)
{
	this->vel = vel;
}

void Boid::set_vel (double vx, double vy)
{
	vel = Vec2d (vx,vy);
}


Vec2d Boid::get_vel () {
    return vel;
}


void Boid::activate (Behaviour b)
{
	active_behaviour = b;
}

Boid::Behaviour Boid::get_active_behaviour () { return active_behaviour; }


void
Boid::set_target (Targetable *tar)
{
    target = tar;
}

/*********** AUXILIARY METHODS **********/
Vec2d
Boid::parse_target ()
{
    return Vec2d(target->x(), target->y());
}

/********* IA LOGIC LAYERS  ************/
Vec2d
Boid::seek (Boid * const self)
{
    Vec2d target = parse_target ();
    Vec2d separation = target.subs (self->pos);
    Vec2d dir = separation.unit ();
    Vec2d desired_vel = dir.mul (VMAX);
    Vec2d accel = desired_vel.subs (vel);
    accel = accel.unit().mul(AMAX);

    return accel;
}

Vec2d
Boid::flee (Boid * const self)
{
    Vec2d target = parse_target ();
    Vec2d separation = target.subs (self->pos);
    Vec2d dir = separation.unit ();
    Vec2d desired_vel = dir.mul (-VMAX);
    Vec2d accel = desired_vel.subs (vel);
    accel = accel.unit().mul(AMAX);

    return accel;
}

BehaviourFn Boid::behaviour_fn[Boid::TOTAL_B+1] = {
             &Boid::seek,
             &Boid::flee,
             NULL
};


Vec2d
Boid::desired_accel ()
{
    return (this->*behaviour_fn[active_behaviour])(this);
}
