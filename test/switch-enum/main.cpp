#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "string"

using namespace std;

enum Options {
    Option1,
    Option2,
    Option_Invalid,
    //others...
};

Options resolveOption(std::string input) {
    if( input == "option1" ) return Option1;
    if( input == "option2" ) return Option2;
    //...
    return Option_Invalid;
}

    int
main ()
{
    string input;

    cin >> input;

    switch( resolveOption(input) )
    {
        case Option1:
            cout << "Option 1\n";
            break;

        case Option2:
            cout << "Option 2\n";
            break;

        // handles Option_Invalid and any other missing/unmapped cases
        default:
            cout << "No option\n";
            break;

    }

}
