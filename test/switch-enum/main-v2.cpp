#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "string"

using namespace std;

enum
Options {
    Option1,
    Option2,
    //others...
};

void
resolveOption (string input) {
    if( input == "option1" ) cout << "Chosen Option 1\n";
    if( input == "option2" ) cout << "Chosen Option 2\n";
    //...
}

    int
main ()
{
    string input;

    cout << "Choose an option [option1, option2]";

    cin >> input;

    resolveOption(input);

    return EXIT_SUCCESS;
}
