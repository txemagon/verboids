#ifndef __CAR_H__
#define __CAR_H__

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;

class Car {
    private:
        string name;
        int x;
        int y;

    public:

        Car (string name, int x=0, int y=0);

        string get_name();

        void set_posX (int Nx);
        int get_posX();

        void set_posY (int Ny);
        int get_posY();
};

#endif
