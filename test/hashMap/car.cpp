#include "car.h"

Car::Car (string Nname, int Nx, int Ny)
{
    name = Nname;
    x = Nx;
    y = Ny;
}

string
Car::get_name() { return name; }

void
Car::set_posX (int Nx) { x = Nx; }

int
Car::get_posX() { return x; }

void
Car::set_posY (int Ny) { y = Ny; }

int
Car::get_posY() { return y; }

