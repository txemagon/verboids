#include <map>
#include <iostream>
#include <stdlib.h>
//#include <cassert>
#include "car.h"
#include <string>
#include <algorithm>

using namespace std;

char *text()
{
    //char *pos = (char *) malloc(10 * sizeof(char));
    string text;
    cin >> text;
    char *pos;
    strcpy(pos, text.c_str() );
    /*std::string s = "Hello World!";

    char cstr[s.size() + 1];

    std::copy(s.begin(), s.end(), cstr);
    cstr[s.size()] = '\0';

    std::cout << cstr << '\n';
*/

    return pos;
}


    int
main(int argc, char **argv)
{
    string nameObject [2];
    string search;
    int posX [2];
    int posY [2];

    // Create a hashmap that the key is the name of the object and the value is the object
    map<string, Car> m;

    int cont = 0;

    while(cont < 2){

        cout << "Insert a name to the new object: ";
        cin >> nameObject[cont];

        cout << "Insert position X: ";
        // cin >> posX[cont];
        //(int) strcpy( posX[cont], text() );
        int foo = atoi (text() );
        posX[cont] = foo;

        cout << "Insert position Y: ";
        cin >> posY[cont];

        // Enter a new data with the name of the object and the creation of the object
        // with the indicated parameters in the hashmap
        m.insert(make_pair<string, Car>( (string) nameObject[cont], Car(nameObject[cont],posX[cont],posY[cont])) );

        cont++;
    }

    map<string, Car>::iterator it = m.begin();

    for(; it != m.end(); it++)
        cout << it->second.get_name() << " :: " << it->first<<endl;

    cout << "Search an object: ";

    cin >> search;

    // Get data of the object in to the hashmap
    cout << "Find first: " << m.find(search)->first << endl;
    cout << "Find get_name: " << m.find(search)->second.get_name() << endl;

    cout << "\nGet position to object "<< m.find(nameObject[0])->first <<": [" << m.find(nameObject[0])->second.get_posX() << "," << m.find(nameObject[0])->second.get_posY() << "]" << endl;
    cout << "\nGet position to object "<< m.find(nameObject[1])->first <<": [" << m.find(nameObject[1])->second.get_posX() << "," << m.find(nameObject[1])->second.get_posY() << "]" << endl;

    cout << "\nGet position to object search "<< m.find(search)->first <<": [" << m.find(search)->second.get_posX() << "," << m.find(search)->second.get_posY() << "]" << endl;

    return 0;
}
