#include <iostream>
#include <fstream>
#include <cstdlib>

#define FILENAME "analyze.txt"
#define PATH_MAX 100
#define MAX      0x100


using namespace std;

const char *analyze_cmd = "analyze -f en.cfg --outlv dep <<< \"";

string
readFile () {

    // Create a text string, which is used to output the text file
    string text;

    // Read from the text file
    ifstream MyReadFile(FILENAME);

    // If the file does not exist, it shows us an error and the program ends
    if(!MyReadFile.is_open()){
        std::cerr << "\033[1;31mERROR\033[0m: Couldn't open the file " << FILENAME << "\n";
        exit (EXIT_FAILURE);
    }

    // Read the file line by line and save it in "text"
    getline(MyReadFile, text);

    // Close the file
    MyReadFile.close();

    return text;
}

int  main(int argc, char *argv[]){

    string text;
    FILE *fp;
    int status;
    char path[PATH_MAX];

    text = readFile();

    fp = popen(analyze_cmd << text <<"\"", "r");   //popen will execute the analyze_cmd command in read mode

    /* If popen fail*/
    if (fp == NULL) {
        /* Handle error */;
        std::cerr << "\033[1;31mERROR\033[0m: Couldn't process the analysis of " << FILENAME << "\n";
        exit (EXIT_FAILURE);
    }

    while (fgets(path, PATH_MAX, fp) != NULL)
        cout << path;

    status = pclose(fp);  //Close popen
    if (status == -1) {
        /* Error reported by pclose() */
    } else {
        /* Use macros described under wait() to inspect `status' in order
           to determine success/failure of command executed by popen() */
    }

    return EXIT_SUCCESS;
}
