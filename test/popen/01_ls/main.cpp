#include <stdio.h>
#include <stdlib.h>
#define PATH_MAX 1000

int
main (int argc, char *argv[])
{

    FILE *fp;
    int status;
    char path[PATH_MAX];


    fp = popen("ls -l", "r");   //popen will execute the ls command in read mode

    /* If popen fail*/
    if (fp == NULL) {
        /* Handle error */;
        fprintf(stderr, "Error to execute ls command");
    }

    while (fgets(path, PATH_MAX, fp) != NULL)
        printf("%s", path);

    status = pclose(fp);  //Close popen
    if (status == -1) {
        /* Error reported by pclose() */
    } else {
        /* Use macros described under wait() to inspect `status' in order
           to determine success/failure of command executed by popen() */
    }

    return EXIT_SUCCESS;
}
