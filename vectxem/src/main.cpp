#include <stdlib.h>
#include <map>

#include "vec2d.h"
#include "boid.h"
#include "hi.h"
#include "mmap.h"

#define MAX 2

/* Function:
 * ------------------
 *  - Main function of the program, function
 *    that is executed when the program starts
 *  - Initialize the program with the HI init function
 *  - Loop that repeats indefinitely
 */
int
main (int argc, char *argv[])
{

    /* Data Input Block */
    Vec2d pos, vel, tar;
    Targetable *target;

    map<string, Boid> boidObjects;
    string command;

    //  pos.set( HI::ask_coord(
    //              "X [Boid]: ",
    //              "Y [Boid]: "
    //              ));
    //
    //  vel.set ( HI::ask_coord(
    //              "Vx [Boid]: ",
    //              "Vy [Boid]: "
    //          ));
    //
    //  tar = HI::ask_coord(
    //              "X [Target]: ",
    //              "Y [Target]: "
    //              );

    HI::welcome( (char *) "mmap/samantha.scr", 7000, true, 10, 0);
    HI::init ();

    /* Function:
     * ------------------
     *  - Loop that repeats indefinitely to run
     *    the program continuously and interact
     *    with the user.
     */
    try {
        while (1)
        {
            command = HI::prompt_command ();

            map<string, Boid>::iterator it = boidObjects.begin();

            for(; it != boidObjects.end(); it++)
                cout << it->first<<endl;
                //cout << it->first << "->" << HI::show_vector("Position", it->second.get_pos()) << endl;

            if(command == "Salir"){
                system("clear");
                return EXIT_SUCCESS;
            }

            if(command == "Menu")
                HI::canva(0);

            if(command == "1")
                HI::canva(1);

            if(command == "Crear boid"){
                char reply;
                int posX = 0, posY = 0;
                string nameObject;

                cout << "Insert a name to the new object: ";
                nameObject = HI::prompt_command ();

                while (reply == 'y' || reply == 'n'){
                    cout << "Do you want to insert a position in the Boid or by default? [y/n]: ";
                    command = HI::prompt_command ();
                    reply = (char) command;
                }

                if(reply == 'y'){
                    cout << "Insert position X: ";
                    posX = (int) HI::prompt_command ();

                    cout << "Insert position Y: ";
                    posY = (int) HI::prompt_command ();
                }

                // Enter a new data with the name of the object and the creation of the object
                // with the indicated parameters in the hashmap
                boidObjects.insert(make_pair<string, Boid>( (string) nameObject, Boid( Vec2d(posX,posY) )) );
            }


//                SI DESCOMENTAS LO DE DEBAJO Y COMENTAS LO DE ARRIBA
//                SE QUEDA COLGADO A LA HORA DE INTRODUCIR LA POSICION


//            command = HI::prompt_command ();
//
//            /*map<string, Boid>::iterator it = boidObjects.begin();
//
//            for(; it != boidObjects.end(); it++)
//                cout << it->first<<endl;
//                //cout << it->first << "->" << HI::show_vector("Position", it->second.get_pos()) << endl;
//*/
//            if(command == "Salir"){
//                system("clear");
//                return EXIT_SUCCESS;
//            }
//
//            if(command == "Menu")
//                HI::canva(0);
//
//            if(command == "1")
//                HI::canva(1);
//
//            if(command == "Crear boid"){
//                char reply = 'y';
//                int posX = 0, posY = 0;
//                string nameObject;
//
//                HI::canva_customize("Insert a name to the new object");
//                nameObject = HI::prompt_command ();
//
//                while (reply == 'y' || reply == 'n'){
//                    HI::canva_customize("Do you want to insert a position in the Boid or by default? [y/n]: ");
//                    command = HI::prompt_command ();
//                    stringstream data(command);
//                    data >> reply;
//                }
//
//                if(reply == 'y'){
//                    HI::canva_customize("Insert position X: ");
//                    command =  HI::prompt_command ();
//                    stringstream data(command);
//                    data >> posX;
//
//                    HI::canva_customize("Insert position Y: ");
//                    command = HI::prompt_command ();
//                    stringstream dataY(command);
//                    dataY >> posY;
//                }
//
//            }
//
//                // Enter a new data with the name of the object and the creation of the object
//                // with the indicated parameters in the hashmap
//               // boidObjects.insert(make_pair<string, Boid>( (string) nameObject, Boid( Vec2d(posX,posY) )) );

            /*switch( HI::resolveOption (command) )
              {
              case (HI::Options) Salir:
              system("clear");
              return EXIT_SUCCESS;

              case Menu:
              HI::canva(0);
              break;
              }*/
        }
    } catch (...){
    }

    //  target = (Targetable *) new Vec2d(tar);
    //  int option = HI::menu ();
    //
    //  Boid manolito (pos, vel);
    //  manolito.set_target (target);
    //
    //  manolito.activate( (Boid::Behaviour) option);
    //  Vec2d accel = manolito.desired_accel();
    //
    //
    //  /* Data Output Block */
    //  system ("clear");
    //  HI::show_vector ("Position", pos);
    //  HI::show_vector ("Velocity", vel);
    //  HI::show_vector ("Target P", tar);
    //  HI::show_vector ("Acceleration", accel);

    return EXIT_SUCCESS;
}
