#ifndef __VEC2D_H__
#define __VEC2D_H__

#include "targetable.h"

class Vec2d : public Targetable
{
private:
  /* Internal storage of coordinates.
     kept private to trigger calculations
     on value change.
   */
  double _x, _y;
  bool _unitarian;

  /* Calculated and cached values for speeding up */
  double _module;
  Vec2d *_unit;
  void do_module ();
  void do_unit ();
  void refresh ();

public:

  /* Constructors & Destructors */
    Vec2d ();                // Default constructor
    Vec2d (const Vec2d &v);  // Copy constructor
    Vec2d (double val_x, double val_y = 0, bool unitr = false);
    ~Vec2d ();

  /* Accessors */
  bool is_unitarian() const;
  double x () const;		// When two methods share name its called overloading (1)
  void x (double val);		// invocation is discriminated against parameters (number and type of) in function call
  double y () const;		// Getter
  void y (double val);		// Setter
  void set (const Vec2d &other);

  double module () const;	// Returns the precalculated module of the vector. It actually acts as getter.
  Vec2d unit () const;		// Returns the precalculated unitarian vector. It is also a getter.

  /* Operations */
  Vec2d mul (double k);		        // Yields a scaled version of the vector.
  Vec2d add (const Vec2d &other);	// Returns the addition. (2)
  Vec2d subs (const Vec2d &other);      // Indeed not necessary. Just 4 convinience.
  double dot (const Vec2d &other);	// Returns the dot product of another vector with self.
  Vec2d cross (const Vec2d &other);	// Returns the cross product of another vector with self.
};

/*
 * (1): const indicates function won't change any value.
 * (2): const Type & is named "const reference" it means: "I don't need a copy of the object
 *      and you just can pass me the pointer (reference) as long as I'm a good boy and I won't change the value.
 */

#endif
