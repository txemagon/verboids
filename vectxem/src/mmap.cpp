#include "mmap.h"
#include "ansi.h"

/* Input parameters:
 * ------------------
 *  - A const text (pointer to the position of the first letter of the file name)
 *  - Int to indicate the length of the file
 *  - Int to indicate cursor position X
 *  - Int to indicate cursor position Y
 *
 * Function:
 * ------------------
 *  - Map the indicated file with mmap
 *  - Move the cursor to the indicated position
 *  - Transforms the read data into the characters indicated
 */

void
MMAP::mmap_file(char *file, int FILE_LENGTH, int x, int y){
    int fichero;
    char *car, *p;

    /* Open file:
     *  - file: File name
     *  - O_RDWR: Open for reading and writing
     *  - O_CREAT: If the file exists, this flag has no effect. Otherwise, the file shall be created
     *  - S_IRUSR: Read permission, owner
     *  - S_IWUSR: Write permission, owner
     *
     * Return a non-negative integer representing the lowest numbered unused file descriptor
     */
    fichero = open (file, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

    /* Creat a new mapping in the virtual address space of the calling process.
     * The starting address for the new mapping is specified in addr:
     *  - 0: address to mapping
     *  - FILE_LENGTH: Lenght to the file
     *  - PROT_WRITE: Pages may be written.
     *  - MAP_SHARED: Updates to the mapping are visible to other processes mapping the same region
     *  - fichero: descriptor int
    */
    p = car = (char *) mmap (0, FILE_LENGTH, PROT_WRITE, MAP_SHARED, fichero, 0);
    close (fichero);

    system("clear");

    MOVE(x,y)   // Move the cursor pointer

    ANSI_SWITCH_COLOR(AC_YELLOW, FORE_NORMAL) // Change the ANSI color to Yellow

    // Loop to print the map by changing the values to represent what we like
    for (int i=0; i<FILE_LENGTH; i++, p++)
        if ( (*p == ';') || (*p == ',') )
            cout << " ";
        else if ( (*p == 'c') || (*p == 'l') ||
                  (*p == 'k') || (*p == 'K') ||
                  (*p == 'X') || (*p == 'x') ||
                  (*p == 'o') || (*p == 'd') ||
                  (*p == '0') || (*p == 'O') )
            cout << "█";
        else
            cout << *p;

    ANSI_RESET;   // Reset the ANSI color

    munmap (car, FILE_LENGTH);    // Close mmap
}
