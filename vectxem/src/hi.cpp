#include "hi.h"

using namespace std;

/* Input parameters:
 * ------------------
 *  - A const text (pointer to the position of the first letter of the text)
 *
 * Function:
 * ------------------
 *  - Ask the coordinates of the object, save the first valie in X ant the second value in Y
 *  - Create a Vec2d object with the values of the X and Y
 *
 * Return:
 * ------------------
 *  - Returns the Vec2d object
 */
    Vec2d
HI::ask_coord (const char *label1, const char *label2)
{
    double x, y;

    system ("clear");
    cout << label1;
    cin >> x;
    cout << label2;
    cin >> y;

    Vec2d data(x, y);

    return data;
}

/* Function:
 * ------------------
 *  - Show a menu with the options that an object can make, behaviors
 *
 * Return:
 * ------------------
 *  - Returns the chosen option
 */
    void
HI::menu ()
{
    //system ("clear");
    cout << "1.- Buscar el objetivo" << endl;
    cout << "2.- Huir del objetivo" << endl;
    cout << "3.- Salir \n" << endl;
    cout << "Elige una opcion: ";
}

/* Input parameters:
 * ------------------
 *  - A const text (pointer to the position of the first letter of the text)
 *  - A Vec2d object
 *
 * Function:
 * ------------------
 *  - Show the X and Y values of the objects
 */
    void
HI::show_vector (const char *label, Vec2d v)
{
    cout << endl << "-> " << label << ": [" << v.x () <<"," << v.y() <<"]" << endl;
}

/* Function:
 * ------------------
 *  - Print the program homepage
 *  - Show the program logo by mapping the "samantha.scr" file with mmap
 */
    void
HI::welcome(char *file, int FILE_LENGTH, bool stop, int x, int y)
{
    if(stop) {
        MMAP::mmap_file(file, FILE_LENGTH, x, y);
        cin.ignore();
    }
    else
        MMAP::mmap_file(file, FILE_LENGTH);
}

/* Function:
 * ------------------
 *  - Initialize the program by cleaning the screen and move
 *    the prompt to the indicated position
 */
    void
HI::init ()
{
    system ("clear");
    welcome( (char *) "mmap/title.scr", 1500, false);
    MOVE_TO(10,0)
}

void
HI::menu_option1 () {
    cout << "Has elegido la opcion 1\n";
}

void
HI::canva_customize (const char *label)
{
    clean_canva();
    cout << label;
}

/* Function:
 * ------------------
 *  - Clean the canva to show new data
 */
void
HI::clean_canva (){

    MOVE_TO(10,0)   // In this position the canva begins

    // Clean the canva line by line to position 50
    for(int i = 0; i < 50; i++) {
        cout << "                                                     ";
        MOVE_TO_CLEAN(10+i)
    }

    // Return the pointer to the beginning of the canva
    MOVE_TO(10,0)
}

/* Function:
 * ------------------
 * - Create a canva that will start on line 10
 *   and end on line 49 where the data will be displayed
 */
void
HI::canva (int n) {

    clean_canva();
    switch(n){
        case 0:
            menu();
            break;
        case 1:
            menu_option1();
            break;
    }
}

/*HI::Options
HI::resolveOption (string input) {
    if(input == "Menu") return Menu;
    if(input == "Salir") return Salir;

    return NULL;

}*/
/* Function:
 * ------------------
 *  - Create a prompt to write instructions and return that instruction
 *
 * Return:
 * ------------------
 *  - Returns the instruction
 */
    string
HI::prompt_command ()
{
    MOVE_TO(50,0)
    // Clean the prompt
    cout << "                                                                             \r";

    // Change the color of the prompt to green
    ANSI_SWITCH_COLOR(AC_GREEN, FORE_NORMAL);

    // Create a prompt and save the instructions we write in buf, reset color ANSI
    char *buf = readline ("OS-1 : Samantha > " ANSI_RESET);

    UP(1)

    // If the user has not written anything is ignored
    if ( buf == nullptr)
        throw;

    // Save the instruction in the history
    if ( strlen (buf) > 0)
        add_history (buf);

    // Convert char array to string (is the same of "string command = buf")
    string command (buf);

    // Free the buf from memory
    free (buf);

    return command;
}
