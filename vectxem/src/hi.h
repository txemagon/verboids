#ifndef __HI_H__
#define __HI_H__

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "string"
#include <readline/readline.h>
#include <readline/history.h>


#include "ansi.h"
#include "vec2d.h"
#include "boid.h"
#include "mmap.h"

#define MOVE_TO(x,y)        cout << "\x1B[" #x ";" #y "f";            // Move the promt pointer to the indicated position
#define MOVE_TO_CLEAN(x)    cout << "\x1B[" #x;                       // Move the promt pointer to the indicated position
#define UP(x)               cout << "\x1B[" #x "A";                   //
#define TITLE               system("toilet -f mono9 '   VERBOIDS' "); // Title of the program

using namespace std;

class HI
{

    public:
        enum Options {Menu, Salir};

   /* Interfaz antiguo */
   static Vec2d ask_coord (const char *label1, const char *label2); // Ask the coordinates of the object
   static void menu ();                                   // Show a menu with the options that an object can make, behaviors
   static void menu_option1 ();                           // Test menu
   static void canva_customize (const char *label);
   static void clean_canva();                             // Clean the canva to print new data
   static void canva (int n);                             // Canva to show the menus and exits of the program
   static Options resolveOption (string input);
   static void show_vector (const char *label, Vec2d v);  // Show the X and Y values of the objects

   /* Interfaz nuevo */
   static void welcome(char *file, int FILE_LENGTH, bool stop, int x = 0, int y = 0); // Print the home screen
   static void init ();             // Initialize the program by cleaning the screen and moving the prompt to the indicated position
   static string prompt_command (); // Create a prompt to write instructions and return that instruction


};

#endif
