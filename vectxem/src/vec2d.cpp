#include <math.h>
#include <stdlib.h>
#include "vec2d.h"


void
Vec2d::do_unit ()
{
    delete _unit;

    if (_unitarian)
       _unit = new Vec2d (1,1, true);
    else
        _unit = new Vec2d (_x / _module, _y / _module, true);

}

void
Vec2d::do_module ()
{
  if (_unitarian)
    _module = 1;
  else
    _module = sqrt (_x * _x + _y * _y);
}

void
Vec2d::refresh ()
{
  if (!_unitarian)
    {
      do_module ();
      do_unit ();
    }
}

Vec2d::Vec2d ()
{
    _x = 0;
    _y = 0;
    _module = 0;
    _unit = new Vec2d (1,0, true);
    _unitarian = false;
}

Vec2d::Vec2d (const Vec2d &v)
{
    _x = v.x ();
    _y = v.y ();
    _module = v.module ();
    Vec2d tmp = v.unit ();
    _unit = new Vec2d (tmp.x (), tmp.y ());
    _unitarian = v.is_unitarian();
}


Vec2d::Vec2d (double val_x, double val_y, bool unitr):
_x (val_x),
_y (val_y),
_unitarian (unitr)
{
  do_module ();
  if (!_unitarian)
      _unit = new Vec2d (_x / _module, _y / _module, true);
  else
      _unit = NULL;
}

Vec2d::~Vec2d ()
{

     if (!_unitarian && _unit) {
         delete _unit;
         _unit = NULL;
     }
}

/* Accessors */
bool
Vec2d::is_unitarian() const
{
    return _unitarian;
}


double
Vec2d::x () const		// Const stands for: "I promise I won't change any value"
{
    return _x;
}

    void
Vec2d::x (double val)
{
    _x = val;
    refresh ();
}

double
Vec2d::y () const
{
    return _y;
}

    void
Vec2d::y (double val)
{
    _y = val;
    refresh ();
}

    void
Vec2d::set (const Vec2d &other)
{
    _x = other.x();
    _y = other.y();
    refresh ();
}

    double
Vec2d::module () const
{
    return _module;
}

    Vec2d
Vec2d::unit () const
{
    if (_unitarian)
        return Vec2d(1,1);
    return *_unit;
}

/* Operations */
Vec2d Vec2d::mul (double k)
{
    return Vec2d (k * _x, k * _y);
}

Vec2d Vec2d::add (const Vec2d & other)
{
    return Vec2d (_x + other.x (), _y + other.y ());
}


Vec2d Vec2d::subs (const Vec2d & other)
{
    return Vec2d (_x - other.x (), _y - other.y ());
}

    double
Vec2d::dot (const Vec2d & other)
{
    return _x * other.x () + _y * other.y ();
}

Vec2d Vec2d::cross (const Vec2d & other)
{
    return Vec2d (_x * other.y () - _y * other.x ());
}
