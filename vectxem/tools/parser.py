#!/usr/bin/python

import nltk
nltk.download('punkt')                          # Word tokenizer
nltk.download('averaged_perceptron_tagger')     # Position Of Text Tagging
nltk.download('maxent_ne_chunker')              # Entity Detection
nltk.download('words')                          # Entity Detection Dependency

str="A brave Scottish general named Macbeth receives a prophecy from a trio of witches that one day he will become King of Scotland. Consumed by ambition and spurred to action by his wife, Macbeth murders King Duncan and takes the Scottish throne for himself. He is then wracked with guilt and paranoia. Forced to commit more and more murders to protect himself from enmity and suspicion, he soon becomes a tyrannical ruler. The bloodbath and consequent civil war swiftly take Macbeth and Lady Macbeth into the realms of madness and death. "

# from nltk.tokenize import sent_tokenize
# print(sent_tokenize(src))

words = nltk.word_tokenize(str)
pos_tags = nltk.pos_tag(tokens)
print(nltk.ne_chunk(pos_tags, binary=True))


# En español
str="El examen de servicios ha concluido catastroficamente. No obstante haremos todo lo que podamos por ayudar a la gente."

from nltk.stem import SnowballStemmer
sp_stemmer = SnowballStemmer('spanish')
print(sp_stemmer.stem(str))


# cess_esp............ CESS-ESP Treebank
# inaugural........... C-Span Inaugural Address Corpus
# spanish_grammars.... Grammars for Spanish
# https://nlp.stanford.edu/software/tagger.shtml
# https://nlp.stanford.edu/software/spanish-faq.shtml#tagset
# http://www.corpus.unam.mx/cursopln/plnPython/clase10.pdf


from nltk.tag import StanfordPOSTagger
tagger="../vendor/stanford-postagger-full-2018-10-16/models/spanish.tagger"
jar="../vendor/stanford-postagger-full-2018-10-16/stanford-postagger.jar"

words = nltk.word_tokenize(str)

etiquetador=StanfordPOSTagger(tagger,jar)
etiquetas=etiquetador.tag(tokens)
