Ní ìbẹ̀rẹ̀ ohun gbogbo Ọlọ́run dá àwọn ọ̀run àti ayé. Ayé sì wà ní rúdurùdu, ó
sì ṣófo, òkùnkùn sì wà lójú ibú omi, Ẹ̀mí Ọlọ́run sì ń rábàbà lójú omi gbogbo.
Ọlọ́run sì wí pé, “Jẹ́ kí ìmọ́lẹ̀ kí ó wà,” ìmọ́lẹ̀ sì wà. Ọlọ́run rí i pé
ìmọ́lẹ̀ náà dára, ó sì ya ìmọ́lẹ̀ náà sọ́tọ̀ kúrò lára òkùnkùn. Ọlọ́run sì pe
ìmọ́lẹ̀ náà ní “Ọ̀sán” àti òkùnkùn ní “Òru.” Àsálẹ́ àti òwúrọ̀ sì jẹ́ ọjọ́
kìn-ín-ní. Ọlọ́run sì wí pé “Jẹ́ kí òfuurufú kí ó wà ní àárin àwọn omi láti
pààlà sí àárin àwọn omi.” Ọlọ́run sì dá òfuurufú láti ya omi tí ó wà ní òkè
òfuurufú kúrò lára omi tí ó wà ní orí ilẹ̀. Ó sì rí bẹ́ẹ̀. Ọlọ́run sì pe
òfúurufú ní “Ọ̀run,” àsálẹ́ àti òwúrọ̀ sì jẹ́ ọjọ́ kejì. Ọlọ́run sì wí pé, “Jẹ́
kí omi abẹ́ ọ̀run wọ́ papọ̀ sí ojúkan, kí ilẹ̀ gbígbẹ sì farahàn.” Ó sì rí
bẹ́ẹ̀. Ọlọ́run sì pe ilẹ̀ gbígbẹ náà ní “Ilẹ̀” àti àpapọ̀ omi ní “Òkun:” Ọlórun
sì rí i wí pé ó dára. Ọlọ́run sì wí pé, “Jẹ́ kí ilẹ̀ kí ó hu ọ̀gbìn: ewéko tí
yóò máa mú èṣo wá àti igi tí yóò máa so èṣo ní irú tirẹ̀, tí ó ní irúgbìn nínú.”
Ó sì rí bẹ́ẹ̀. Ilẹ̀ sì hù ọ̀gbìn: ewéko tí ó ń so èṣo ní irú tirẹ̀, àti igi tí ń
so èṣo, tí ó ní irúgbìn nínú ní irú tirẹ̀. Ọlọ́run sì ri pé ó dára. Àsálẹ́ àti
òwúrọ̀ jẹ́ ọjọ́ kẹta. Ọlọ́run sì wí pé “Jẹ́ kí ìmọ́lẹ̀ kí ó wà ní ojú ọ̀run,
làti pààlà sí àárin ọ̀sán àti òru, kí wọn ó sì máa wà fún àmì láti mọ àwọn
àsìkò, àti àwọn ọjọ́ àti àwọn ọdún, Kí wọn ó jẹ́ ìmọ́lẹ̀ ní ojú ọ̀run, láti tan
ìmọ́lẹ̀ sí orí ilẹ̀.” Ó sì rí bẹ́ẹ̀. Ọlọ́run dá ìmọ́lẹ̀ ńlá-ńlá méjì, ìmọ́lẹ̀ tí
ó tóbi láti ṣe àkóso ọ̀sán àti ìmọ́lẹ̀ tí ó kéré láti ṣe àkóso òru. Ó sì dá àwọn
ìràwọ̀ pẹ̀lú. Ọlọ́run sì ṣe wọ́n lọ́jọ̀ sí ojú ọ̀run láti máa tan ìmọ́lẹ̀ si orí
ilẹ̀, láti ṣàkóso ọ̀sán àti òru àti láti pààlà sí àárin ìmọ́lẹ̀ àti òkùnkùn:
Ọlọ́run sì rí i pé ó dára. Àsálẹ́ àti òwúrọ jẹ́ ọjọ́ kẹrin. Ọlọ́run sì wí pé,
“Jẹ́ kí omi kí ó kún fún àwọn ohun alààyè, kí àwọn ẹyẹ kí ó sì máa fò ní
òfuurufú.” Nítorí náà Ọlọ́run dá àwọn ẹ̀dá alààyè ńlá-ńlá sí inú òkun, àwọn ohun
ẹlẹ́mìí àti àwọn ohun tí ń rìn ní onírúurú tiwọn, àti àwọn ẹyẹ abìyẹ́ ní
onírúurú tiwọn. Ọlọ́run sì rí i pé ó dára. Ọlọ́run súre fún wọn, ó sì wí pé, “Ẹ
máa bí sí i, ẹ sì máa pọ̀ sí i, ẹ kún inú omi òkun, kí àwọn ẹyẹ náà sì máa pọ̀
sí i ní orí ilẹ̀.” Àsálẹ́ àti òwúrọ̀ jẹ́ ọjọ́ kárùn-ún. Ọlọ́run sì wí pé, “Kí
ilẹ̀ kí ó mú ohun alààyè jáde ní onírúurú wọn: ẹran ọ̀sìn, àwọn ohun afàyàfà àti
àwọn ẹran inú igbó, ọ̀kọ̀ọ̀kan ní irú tirẹ̀.” Ó sì rí bẹ́ẹ̀. Ọlọ́run sì dá
ẹranko inú igbó àti ẹran ọ̀sìn gbogbo ní irú tirẹ̀ àti gbogbo ohun alààyè tí ń
rìn ní ilẹ̀ ní irú tirẹ̀. Ọlọ́run sì rí i pé ó dára. Lẹ́yìn náà ni Ọlọ́run wí
pé, “Ẹ jẹ́ kí a dá ènìyàn ní àwòrán ara wa, gẹ́gẹ́ bí àwa ti rí, kí wọn kí ó
jọba lórí ẹja òkun, ẹyẹ ojú ọ̀run, ohun ọ̀sìn, gbogbo ilẹ̀ àti lórí ohun gbogbo
tí ń rìn lórí ilẹ̀.” 

“Lóòótọ́, lóòótọ́ ni mo wí fún yín, Ẹni tí kò bá gba ẹnu ọ̀nà wọ inú agbo
àgùntàn, ṣùgbọ́n tí ó bá gba ibòmíràn gun òkè, Òun náà ni olè àti ọlọ́sà.
Ṣùgbọ́n ẹni tí ó bá ti ẹnu-ọ̀nà wọlé, Òun ni olùṣọ́ àwọn àgùntàn. Òun ni osọ́nà
yóò ṣílẹ̀kùn fún; àwọn àgùntàn gbọ́ ohùn rẹ̀: ó sì pe àwọn àgùntàn tirẹ̀ lórúkọ,
ó sì se amọ̀nà wọn jáde. Nígbà tí ó bá sì ti mú àwọn àgùntàn tirẹ̀ jáde, yóò
ṣíwájú wọn, àwọn àgùntàn yóò sì máa tọ̀ ọ́ lẹ́yìn: nítorí tí wọ́n mọ ohùn rẹ̀.
Wọn kò jẹ́ tọ àlejò lẹ́yìn, ṣùgbọ́n wọn a sá kúrò lọ́dọ̀ rẹ̀: nítorí tí wọn kò
mọ ohùn àlejò.” Òwe yìí ni Jésù pa fún wọn: ṣùgbọ́n òye ohun tí nǹkan wọ̀nyí jẹ́
tí ó ń sọ fún wọn kò yé wọn. Nítorí náà Jésù tún wí fún wọn pé, “Lóòótọ́,
lóòótọ́ ni mo wí fún yín, Èmi ni ìlẹ̀kùn àwọn àgùntàn. Olè àti ọlọ́sà ni gbogbo
àwọn tí ó ti wà ṣáájú mi: ṣùgbọ́n àwọn àgùntàn kò gbọ́ ti wọn. Èmi ni ìlẹ̀kùn:
bí ẹnìkan bá bá ọ̀dọ̀ mi wọlé, Òun ni a ó gbà là, yóò wọlé, yóò sì jáde, yóò sì
rí koríko. Olè kìí wá bí kò ṣe láti jalè, láti pa, àti láti parun: èmi wá kí wọn
lè ní ìyè, àní kí wọn lè ní i lọ́pọ̀lọpọ̀. “Èmi ni olùṣọ́ àgùntàn rere:
olùṣọ́-àgùntàn rere fi ọkàn rẹ̀ lélẹ̀ nítorí àwọn àgùntàn. Ṣùgbọ́n alágbàṣe, tí
kìí ṣe olùṣọ́ àgùntàn, ẹni tí àwọn àgùntàn kìí ṣe tirẹ̀, ó rí ìkokò ń bọ̀, ó sì
fi àgùntàn sílẹ̀, ó sì fọ́n wọn ká kiri. Òun sá lọ nítorí tí ó jẹ́ alágbàṣe, kò
sì náání àwọn àgùntàn. Èmi sì ní àwọn àgùntàn mìíràn, tí kìí ṣe agbo yìí: àwọn
ni èmi yóò mú wá pẹ̀lú, wọn ó sì gbọ́ ohùn mi; wọn ó sì jẹ́ agbo kan,
olùsọ́-àgùntàn kan. Nítorí náà ni Baba mi ṣe fẹ́ràn mi, nítorí tí mo fi ẹ̀mí mi
lélẹ̀, kí èmi lè tún gbà á. Ẹnìkan kò gbà á lọ́wọ́ mi, ṣùgbọ́n mo fi í lélẹ̀, mo
sì lágbára láti tún gbà á. Àṣẹ yìí ni mo ti gbà wá láti ọ̀dọ̀ Baba mi.” Nítorí
náà ìyapa tún wà láàrin àwọn Júù nítorí ọ̀rọ̀ wọ̀nyí. Ọ̀pọ̀ nínú wọn sì wí pé,
“Ó ní ẹ̀mí èṣù, orí rẹ̀ sì dàrú; èéṣe tí ẹ̀yin fi ń gbọ́rọ̀ rẹ̀?” Àwọn mìíràn wí
pé, “Ìwọ̀nyí kìí ṣe ọ̀rọ̀ ẹni tí ó ní ẹ̀mí èṣù. Ẹ̀mi èṣù lè la ojú àwọn afọ́jú
bí?” Ó sì jẹ́ àjọ̀dún ìyàsímímọ́ ní Jérúsálẹ́mù, ìgbà òtútù ni. Jésù sì ń rìn ní
tẹ́mpílì, ní ìloro Sólómónì, Nítorí náà àwọn Júù wá dúró yí i ká, wọ́n sì wí fún
un pé, “Ìwọ ó ti mú wa ṣe iyèméjì pẹ́ tó? Bí ìwọ bá ni Kírísítì náà, wí fún wa
gbangba.” Jésù dá wọn lóhùn pé, “Èmi ti wí fún yín, ẹ̀yin kò sì gbàgbọ́; iṣẹ́ tí
èmi ń ṣe lórúkọ Baba mi, àwọn ni ó ń jẹ́rìí mi. Ṣùgbọ́n ẹ̀yin kò gbàgbọ́, nítorí
ẹ̀yin kò sí nínú àwọn àgùntàn mi, gẹ́gẹ́ bí mo tí wí fún yín. Baba mi, ẹni tí ó
fi wọ́n fún mi pọ̀ ju gbogbo wọn lọ; kò sì sí ẹni tí ó lè já wọn gbà kúrò lọ́wọ́
Baba mi. Ọ̀kan ni èmi àti Baba mi.” Àwọn Júù sì tún he òkúta, láti sọ lù ú. Jésù
dá wọn lóhùn pé, “Ọ̀pọ̀lọpọ̀ iṣẹ́ rere ni mo fi hàn yín láti ọ̀dọ̀ Baba mi wá;
nítorí èwo nínú iṣẹ́ wọ̀nyí ni ẹ̀yin ṣe sọ mí ní òkúta?” Àwọn Júù sì dá a lóhùn
pé, “Àwa kò sọ ọ́ lókúta nítorí iṣẹ́ rere, ṣùgbọ́n nítorí ọ̀rọ̀-ọ̀dì: àti nítorí
ìwọ tí í ṣe ènìyàn ń fi ara rẹ pe Ọlọ́run.” Jésù dá wọn lóhùn pé, “A kò ha ti kọ
ọ́ nínú òfin yín pé, ‘Mo ti wí pé, Ọlọ́run ni ẹ̀yin jẹ́’? Bí èmi kò bá ṣe iṣẹ́
Baba mi, ẹ má ṣe gbà mí gbọ́. Ṣùgbọ́n bí èmi bá ṣe wọ́n, bí ẹ̀yin kò tilẹ̀ gbà
mí gbọ́, ẹ gbà iṣẹ́ náà gbọ́: kí ẹ̀yin baà lè mọ̀, kí ó sì lè yé yín pé, Baba wà
nínú mi, èmi sì wà nínú rẹ̀.” Wọ́n sì tún ń wá ọ̀nà láti mú un: ó sì bọ́ lọ́wọ́
wọn. Àwọn ènìyàn púpọ̀ sì wá sọ́dọ̀ rẹ̀, wọ́n sì wí pé, “Jòhánù kò ṣe iṣẹ́ àmì
kan: Ṣùgbọ́n òtítọ́ ni ohun gbogbo tí Jòhánù sọ nípa ti ọkùnrin yìí.” Àwọn
ènìyàn púpọ̀ níbẹ̀ sì gbàágbọ́. 

Ara ọkùnrin kan sì ṣe aláìdá, Lásárù, ará Bẹ́tẹ́nì, tí í ṣe ìlú Màríà àti Mátà
arábìnrin rẹ̀. (Màríà náà ni ẹni tí ó fi òróró ìkunra kun Olúwa, tí ó sì fi irun
orí rẹ̀ nù ún, arákùnrin rẹ̀ ni Lásárù í ṣe, ara ẹni tí kò dá.) Nítorí náà, àwọn
arákùnrin rẹ̀ ránsẹ́ sí i, wí pé, “Olúwa, wò ó, ara ẹni tí ìwọ fẹ́ràn kò dá.”
Nígbà tí Jésù sì gbọ́, ó wí pé, “Àìsàn yìí kìí ṣe sí ikú, ṣùgbọ́n fún Ògo
Ọlọ́run, kí a lè yin Ọmọ Ọlọ́run lógo nípasẹ̀ rẹ̀.” Ṣùgbọ́n bí ẹnìkan bá rìn ní
òru, yóò kọsẹ̀, nítorí tí kò sí ìmọ́lẹ̀ nínú rẹ̀.” Nǹkan wọ̀nyí ni ó sọ: lẹ́yìn
èyí nì ó sì wí fún wọn pé, “Lásárù ọ̀rẹ́ wa sùn; ṣùgbọ́n èmi ń lọ kí èmi kí ó lè
jí i dìde nínú orun rẹ̀.” Nítorí náà àwọn ọmọ-ẹ̀yìn rẹ̀ wí fún un pé, “Olúwa, bí
ó bá se pé ó sùn, yóò sàn.” Ṣùgbọ́n Jésù ń sọ ti ikú rẹ̀: ṣùgbọ́n wọ́n rò pé, ó
ń sọ ti orun sísùn. Nígbà náà ni Jésù wí fún wọn gbangba pé, Lásárù kú, Èmi sì
yọ̀ nítorí yín, tí èmi kò sí níbẹ̀, Kí ẹ le gbàgbọ́; ṣùgbọ́n ẹ jẹ́ kí a lọ
sọ́dọ̀ rẹ̀. “Nítorí náà Tómásì, ẹni tí à ń pè ní Dídímù, wí fún àwọn ọmọ-ẹ̀yìn
ẹgbẹ́ rẹ̀ pé, Ẹ jẹ́ kí àwa náà lọ, kí a lè bá a kú pẹ̀lú.” Nítorí náà nígbà tí
Jésù dé, ó rí i pé a ti tẹ́ ẹ sínú ibojì ní ijọ́ mẹ́rin ná, Ǹjẹ́ Bétanì súnmọ́
Jérúsálẹ́mù tó ibùsọ Mẹ́ẹ̀dógún: Ọ̀pọ̀ nínú àwọn Júù sì wá sọ́dọ̀ Mátà àti Màríà
láti tù wọ́n nínú nítorí ti arákùnrin wọn. Nítorí náà, nígbà tí Mátà gbọ́ pé
Jésù ń bọ̀ wá, ó jáde lọ pàdé rẹ̀: ṣùgbọ́n Màríà jòkó nínú ilé. Nígbà náà, ni
Mátà wí fún Jésù pé, “Olúwa, ìbá ṣe pé ìwọ ti wà níhìn-ín, arákùnrin mi kì bá
kú. Jésù wí fún un pé, “Arákùnrin rẹ yóò jíǹde.” Mátà wí fún un pé, “Mo mọ̀ pé
yóò jíǹde ní àjíǹde ìkẹyìn.” Jésù wí fún un pé, “Èmi ni àjíǹde àti ìyè: ẹni tí ó
bá gbà mí gbọ́, bí ó tilẹ̀ kú, yóò yè: Ẹnikẹ́ni tí ó ń bẹ láàyè, tí ó sì gbà mí
gbọ́, kì yóò kú láéláé ìwọ gbà èyí gbọ́?” Ó wí fún un pé, “Bẹ́ẹ̀ ni, Olúwa: èmi
gbàgbọ́ pé, ìwọ ni Kírísítì náà Ọmọ Ọlọ́run, ẹni tí ń bọ̀ wá sí ayé.” Nígbà tí ó
sì ti wí èyí tán, ó lọ, ó sì pe Màríà arábìnrin rẹ̀ sẹ́yìn wí pé, “Olùkọ́ dé, ó
sì ń pè ọ́.” Nígbà tí ó gbọ́, ó dìde lọ́gán, ó sì wá sọ́dọ̀ rẹ̀. Jésù kò tíì wọ
ìlú, ṣùgbọ́n ó wà ní ibi kan náà tí Mátà ti pàdé rẹ̀. Nígbà tí àwọn Júù tí ó wà
lọ́dọ̀ rẹ̀ nínú ilé, tí wọ́n ń tù ú nínú rí Màríà tí ó dìde kánkán, tí ó sì
jáde, wọ́n tẹ̀lé, wọ́n ṣebí ó ń lọ sí ibojì láti sọkún níbẹ̀. Nígbà tí Màríà sì
dé ibi tí Jésù wà, tí ó sì rí i, ó wólẹ̀ lẹ́bàá ẹsẹ̀ rẹ̀, ó wí fún un pé,
“Olúwa, ìbáṣe pé ìwọ ti wà níhín-ín, arákùnrin mi kì bá kú.” Ó sì wí pé, “Níbo
ni ẹ̀yin gbé tẹ́ ẹ sí?” Wọ́n sì wí fún un pé, “Olúwa, wá wò ó.” Jésù sọkún.
Nítorí náà àwọn Júù wí pé, “Sá wò ó bí ó ti fẹ́ràn rẹ̀ tó!” Àwọn kan nínú wọn sì
wí pé, “Ọkùnrin yìí, ẹni tí ó la ojú afọ́jú, kò lè ṣeé kí ọkùnrin yìí má kú bí?”
Nígbà náà ni Jésù tún kérora nínú ara rẹ̀, ó wá sí ibojì, ó sì jẹ́ ihò, a sì gbé
òkúta lé ẹnu rẹ̀. Jésù wí pé, “Ẹ gbé òkúta náà kúrò!” Màtá, arábìnrin ẹni tí ó
kú náà wí fún un pé, “Olúwa, ó ti ń rùn nísinsin yìí: nítorí pé ó di ijọ́ kẹrin
tí ó tí kú.” Jésù wí fún un pé, “Èmi kò ti wí fún ọ pé, bí ìwọ bá gbàgbọ́, ìwọ ó
rí ògo Ọlọ́run?” Nígbà náà ni wọ́n gbé òkúta náà kúrò (níbi tí a tẹ́ ẹ sí). Jésù
sì gbé ojú rẹ̀ sókè, ó sì wí pé, “Baba, mo dúpẹ́ lọ́wọ́ rẹ nítorí tí ìwọ gbọ́
tèmi. Èmi sì ti mọ̀ pé, ìwọ a máa gbọ́ ti èmi nígbà gbogbo: ṣùgbọ́n nítorí ìjọ
ènìyàn tí ó dúró yìí ni mo ṣe wí i, kí wọn baà lè gbàgbọ́ pé ìwọ ni ó rán mi.”
Nígbà tí ó sì wí bẹ́ẹ̀ tan, ó kígbe lóhùn rara pé, “Lásárù, jáde wá.” Ẹni tí ó
kú náà sì jáde wá, tí a fi aṣọ òkú dì tọwọ́ tẹsẹ̀ a sì fi gèlè dì í lójú. Jésù
wí fún wọn pé, “Ẹ tú u, ẹ sì jẹ́ kí ó má a lọ!” Nítorí náà ni ọ̀pọ̀ àwọn Júù tí
ó wá sọ́dọ̀ Màríà, tí wọ́n rí ohun tí Jésù ṣe, ṣe gbà á gbọ́. Ṣùgbọ́n àwọn
ẹlòmíràn nínú wọn tọ àwọn Farisí lọ, wọ́n sì sọ fún wọn ohun tí Jésù ṣe. 
