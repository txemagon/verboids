# Summary

Erme Universal Dependencies annotated texts Moksha are the origin of UD_Moksha-JR with annotation (CoNLL-U) for texts in the Moksha language,
it originally consists of a sample from a number of fiction authors writing originals in Moksha.


# Introduction

This is a collection of sentences from almost entirely original Moksha-language literary sources dating back to the 1880s with
 Universal Dependencies (UD) annotations. It has been constructed in alignment with parallel work on Erzya language Universal Dependencies.

There are also about 20 parallel sentences translated by Marina Liovina from the Erzya and Russian
texts: [http://ilazki.thinkgeek.co.uk/brat/#/uralic/myv](http://ilazki.thinkgeek.co.uk/brat/#/uralic/myv) and
[http://ilazki.thinkgeek.co.uk/brat/#/uralic/rus](http://ilazki.thinkgeek.co.uk/brat/#/uralic/rus)

The sent_id attribute value is not randomized in works published earlier than 1938. Developing UD documentation can be found at https://github.com/UniversalDependencies/docs for Erzya.

[https://github.com/rueter/erme-ud-moksha](https://github.com/rueter/erme-ud-moksha)


# Acknowledgments

The original annotation has been performed by Jack Rueter at the University of Helsinki with the help of Marina Liovina
at the Mordovian State University im. P.N. Ogariova, Mordvin Languages Department using morphological tools 
that were originally built with funding from a Kone Foundation «Language Programme» funded project: 
«Creation of Morphological Parsers for Minority Finno-Ugrian Languages» (2013–2014) with the linguistic work of
Merja Salo, and facilitated at the Norwegian Arctic University in Tromsø. Work with the Moksha treebank
builds upon previous experience with the UD_Erzya-JR treebank and continued consultations and discussions
with Francis Tyers, Tommi Pirinen, Jonathan Washington. Without the Moksha writers themselves, however, we would be no where…


## References

If you use this data set in an academic publication, I would be ever so grateful if you cited it as follows:

Jack Rueter. (2018, January 20). Erme UD Moksha (Version v1.0) http://doi.org/10.5281/zenodo.1156112

[![DOI](https://zenodo.org/badge/118232421.svg)](https://zenodo.org/badge/latestdoi/118232421)

## About the authors

* Кузнецов, Юрий 1975: Сембось ушеткшни киста. Саранск.
* Mishanina, V. I. (Мишанина, В. И.) 1972: Лиендень очконяса. Мокша №3, 38–39. Саранск. (MishaninaValentina_LiendenyOchkonyasa_Moksha-1972-No2-pp38-39) (Мордовиянь Кадошкина аймаконь Адаж веле)


# Changelog

* 2019-11-15 v2.5
  * Initial release in Universal Dependencies.


<pre>
=== Machine-readable metadata (DO NOT REMOVE!) ================================
Data available since: UD v2.5
License: CC BY-SA 4.0
Includes text: yes
Genre: nonfiction news
Lemmas: converted from manual
UPOS: converted from manual
XPOS: manual native
Features: converted from manual
Relations: converted from manual
Contributors: Rueter, Jack; Liovina, Maria; Kabaeva, Nadezhda
Contributing: here
Contact: rueter.jack@gmail.com
===============================================================================
</pre>
