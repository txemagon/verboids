# Summary

The [Bhojpuri](https://en.wikipedia.org/wiki/Bhojpuri_language) UD Treebank (BHTB) is a part of the [Universal Dependency treebank](http://universaldependencies.org/) project.


# Introduction

The [Bhojpuri](https://en.wikipedia.org/wiki/Bhojpuri_language) UD Treebank (BHTB) v2.5 consists of 5,189 tokens(252 sentences). This Treebank is a part of the [Universal Dependency treebank](http://universaldependencies.org/) project. Initially, it was initiated by me ([Atul](http://ufal.ms.mff.cuni.cz/atul-kr-ojha)) at [Jawaharlal Nehru University, New Delhi](http://sanskrit.jnu.ac.in/index.jsp) during the [doctoral](http://sanskrit.jnu.ac.in/rstudents/phd.jsp) research work. BHTB data contains syntactic annotation according to dependency-constituency schema, as well as morphological tags and lemmas. In this data, XPOS is annotated  according to [Bureau of Indian Standards (BIS) Part Of Speech (POS) tagset](http://tdil-dc.in/tdildcMain/articles/134692Draft%20POS%20Tag%20standard.pdf).


# Acknowledgments

Preparation of this treebank was supported by LINDAT/CLARIAH-CZ (grant no. LM2018051).
[ÚFAL](http://ufal.mff.cuni.cz/), Faculty of Mathematics and Physics, Charles University, Prague.

## References

<pre>
@article{ojha2019english,
  title={English-Bhojpuri SMT System: Insights from the Karaka Model},
  author={Ojha, Atul Kr},
  journal={arXiv preprint arXiv:1905.02239},
  year={2019}
}
</pre>
<pre>
@inproceedings{ojha2015training,
  title={Training \& evaluation of POS taggers in Indo-Aryan languages: a case of Hindi, Odia and Bhojpuri},
  author={Ojha, Atul Kr. and Behera, Pitambar and Singh, Srishti and Jha, Girish N},
  booktitle={the proceedings of 7th Language \& Technology Conference: Human Language Technologies as a Challenge for Computer Science and Linguistics},
  pages={524--529},
  year={2015}
}
</pre>


<pre>
=== Machine-readable metadata (DO NOT REMOVE!) ================================
Data available since: UD v2.5
License: CC BY-SA 4.0
Includes text: yes
Genre: nonfiction news
Lemmas: manual native
UPOS: converted from manual
XPOS: manual native
Features: converted from manual
Relations: manual native
Contributors: Ojha, Atul Kr.; Zeman, Daniel
Contributing: here
Contact: shashwatup9k@gmail.com, zeman@ufal.mff.cuni.cz
===============================================================================
</pre>
