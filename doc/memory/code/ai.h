#ifndef __AI_H__
#define __AI_H__

#include <stdio.h>
#include "string"
#include <vector>
#include <map>
#include <algorithm>


#include <stdlib.h>
#include <iostream>
#include <readline/readline.h>
#include <readline/history.h>
#include <map>


#include "socket.h"

#define SLEEP 80

struct Sentence
{
    std::string subject;
    std::string action;
    std::vector<std::string> compliment;
};


class AI
{
    public:
        static socket_CS *sock;                         // Pointer of conection to the socket
        static void process (std::string command);      // Create a socket connected with Freeling to write and read texts that Freeling parses
        static void *init (void *arg);                  // Properly close the connection to the socket
        static void finalize ();                        // Send command to socket to parse
        static void logic (std::map <std::string, Boid> &, int &, std::string command); // Develops the logic of boid behavior
};

#endif
