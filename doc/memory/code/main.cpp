#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <map>
#include <pthread.h>
#include <algorithm>

#include "vec2d.h"
#include "boid.h"
#include "interface/interface.h"
#include "freeling/freeling.h"
#include "mmap.h"

#define NBOIDS 3   //Number of Boids objects

/* Lambdas: https://www.cprogramming.com/c++11/c++11-lambda-closures.html */

sig_atomic_t run = 1;


 /** Input parameters:
 * ------------------
 *  - Int
 *
 * Function:
 * ------------------
 *  - Change sig_atomic variable to finish the while
*/
void the_quit (int signum) {
    run = 0;
}

/**
 * Initialize language parser server.
 */
void
*initialize (void *arg)
{
    pthread_t thread_progress_bar,    // Define thread prints the loading bar on the loading screen
              thread_ai_init;         // Define thread to start AI

    CURSOR_OFF                        // ANSI function to hide cursor

    struct sigaction sa;
    bzero (&sa, sizeof (sa));
    sa.sa_handler = &the_quit;
    sigaction (SIGTERM, &sa, NULL);

    Freeling::initialize ();                                                  // Initialize Freeling
    HI::welcome ( (char *) "artwork/samantha-load.scr", 7000, false, 5, 0);   // Print loading screen
    pthread_create(&thread_progress_bar, NULL, &LOAD::progress_bar, NULL);    // Use thread progress bar
    pthread_create(&thread_ai_init, NULL, &AI::init, NULL);                   // Use thread ai init
    pthread_join (thread_progress_bar, NULL);                                 // Wait finish thread progress bar
    pthread_join (thread_ai_init, NULL);                                      // Wait finish thread ai init
    HI::welcome ( (char *) "artwork/samantha.scr", 7000, true, 5, 0);         // Print loading scree with "PRESS ENTER" to start

    CURSOR_ON             // ANSI function to show cursor

    HI::init ();          // Print screen to use the program
}

/** Function:
 * ------------------
 *  - Main function to close the program
 *  - Call all functions to close the program properly
 */
void
finalize ()
{
    HI::finalize ();          // ANSI reset
    AI::finalize ();          // Finalice Ai
    Freeling::finalize ();    // Finalice Freeling
    CURSOR_ON
}

/**
 *  - Initialize the program with the HI init function
 *  - Loop that repeats indefinitely
 */
int
main (int argc, char *argv[])
{
    pthread_t thread_initialize,    // Define thread to init the program
              thread_music_play;    // Define thread play music to listen a song

    map<string, Boid> boidObjects;  // Map of boids objects related to a name

    // Create boids and insert them into boidObjects map
    for(int i = 1; i < NBOIDS; ++i)
        boidObjects.insert(make_pair<string, Boid>( "Boid" + to_string(i), Boid(i,i*i+1,i+i*2,i+3,i*3,i*2,i)) );

    string command;       // Save command write
    bool samantha;        // Enable or disable samantha (analyze text)
    int vuelta = 0;       // Count the number of turns in the while

    pthread_create(&thread_music_play, NULL, &LOAD::music_play, NULL);  // Use thread to play music
    pthread_create(&thread_initialize, NULL, &initialize, NULL);        // Use thread init
    pthread_join (thread_initialize, NULL);                             // Wait finish thread init

    while (run)
        {
            try {
                command = HI::prompt_command ();      // Save command write on prompt
                if(command == "help")                 // If command is "help" show the documentation
                    HI::canvas_help();
                if(command == "Activate samantha" || command == "activate samantha" || command == "Activate Samantha")  // If command is "Activa samantha" active analyze text and disable AI boids
                    samantha = true;
                if(command == "Desactivate samantha" || command == "desactivate samantha" || command == "Desactivate Samantha")   // Disable analyze text and active AI boids
                    samantha = false;

                if(samantha){
                    HI::clean_canvas();               // Clean canvas to print new data
                    AI::process(command);             // Analyze the text and display the results
                }
                else {
                    HI::canvas(boidObjects);
                    AI::logic(boidObjects, vuelta, command);  // Process AI boids
                }

                vuelta++;

                if( vuelta >= boidObjects.size() + 1 ) { vuelta = 1; }

                /*
                 * Lambda function. it doesn't work correctly
                 *
                   std::for_each(boidObjects.begin(), boidObjects.end(),
                   [](std::pair<std::string, Boid> element){
                // Accessing KEY from element
                std::string word = element.first;
                // Accessing VALUE from element.
                Boid obj = element.second;
                //std::cout<<word<<std::endl;

                double x,y;

                if( obj.get_pos().x() == obj.get_tar().x() ) { x = obj.get_pos().x(); }
                else if(obj.get_pos().x() > obj.get_tar().x()) { x = obj.get_pos().x() - 1; }
                else { x = obj.get_pos().x() + 1; }

                if ( obj.get_pos().y() == obj.get_tar().y() ) { y = obj.get_pos().y(); }
                else if(obj.get_pos().y() > obj.get_tar().y()) { y = obj.get_pos().y() - 1; }
                else { y = obj.get_pos().y() + 1; }

                obj.set_pos(x, y);
                });
                */
            } catch (...){
            }
        }

    finalize ();

    return EXIT_SUCCESS;
}
