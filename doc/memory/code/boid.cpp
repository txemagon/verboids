#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include "boid.h"

/* ALL BOIDS CHARACTERISTICS */
const double Boid::VMAX = 10;
const double Boid::AMAX = 2;

/********* Constructors *********/
Boid::Boid (double x, double y, double vx, double vy, double tx, double ty, int nVeloz, std::string nName_target, std::string nAction) :
	pos(x,y),
	vel(vx, vy),
        tar(tx, ty)
{
    veloz = nVeloz;
    name_target = nName_target;
    action = nAction;
	//target = NULL;
}

Boid::Boid (Vec2d pos, Vec2d vel, Vec2d tar, int nVeloz, std::string nName_target, std::string nAction) :
	pos(pos),
	vel(vel),
        tar(tar)
{
    veloz = nVeloz;
    name_target = nName_target;
    action = nAction;
//	target = NULL;
}

/**************  Getter and Setters *************/

/* Targetable */
double Boid::x () const {return pos.x (); }
double Boid::y () const {return pos.y (); }

void Boid::set_veloz(int nVeloz)
{
    this->veloz = nVeloz;
}

int Boid::get_veloz()
{
    return veloz;
}

void
Boid::set_name_target(std::string nName_target){
    this->name_target = nName_target;
}

std::string
Boid::get_name_target(){
    return name_target;
}

void
Boid::set_action(std::string nAction){
    action = nAction;
}

std::string
Boid::get_action(){
    return action;
}

void Boid::set_tar(Vec2d tar)
{
    this->tar = tar;
}

void Boid::set_tar(double tx, double ty)
{
    tar.x(tx);
    tar.y(ty);
    //tar = Vec2d(tx, ty);
}

Vec2d Boid::get_tar()
{
    return tar;
}

/* Position */
void Boid::set_pos (Vec2d pos)
{
    this->pos = pos;
}

void Boid::set_pos (double x, double y)
{
    pos.x(x);
    pos.y(y);
    //pos = Vec2d (x,y);
}

Vec2d Boid::get_pos () { return pos;}

/* Velocity */
void Boid::set_vel (Vec2d vel)
{
    this->vel = vel;
}

void Boid::set_vel (double vx, double vy)
{
    vel = Vec2d (vx,vy);
}


Vec2d Boid::get_vel () {
    return vel;
}


void Boid::activate (Behaviour b)
{
    active_behaviour = b;
}

Boid::Behaviour Boid::get_active_behaviour () { return active_behaviour; }


    void
Boid::set_target (Targetable *tar)
{
    target = tar;
}

    void
Boid::set_random_target()
{
    srand( (unsigned)time(NULL) );

    double x = rand() % 39 + 1;
    double y = rand() % 145 + 1;

    this->set_tar(x, y);
}

/*********** AUXILIARY METHODS **********/
    Vec2d
Boid::parse_target ()
{
    return Vec2d(target->x(), target->y());
}

/********* IA LOGIC LAYERS  ************/
    Vec2d
Boid::seek (Boid * const self)
{/*
    Vec2d target = parse_target ();
    Vec2d separation = target.subs (self->pos);
    Vec2d dir = separation.unit ();
    Vec2d desired_vel = dir.mul (VMAX);
    Vec2d accel = desired_vel.subs (vel);
    accel = accel.unit().mul(AMAX);

    return accel;*/
}

    Vec2d
Boid::flee (Boid * const self)
{
    Vec2d target = parse_target ();
    Vec2d separation = target.subs (self->pos);
    Vec2d dir = separation.unit ();
    Vec2d desired_vel = dir.mul (-VMAX);
    Vec2d accel = desired_vel.subs (vel);
    accel = accel.unit().mul(AMAX);

    return accel;
}

    Vec2d
Boid::walk (Boid * const self)
{
    set_random_target();
    //Vec2d target = parse_target ();
    Vec2d target = self->get_tar();
    /*Vec2d separation = target.subs (self->pos);
      Vec2d dir = separation.unit ();
      Vec2d desired_vel = dir.mul (VMAX);
      Vec2d accel = desired_vel.subs (vel);
      accel = accel.unit().mul(AMAX);

      return accel;*/

    return Vec2d(5,5);
}

BehaviourFn Boid::behaviour_fn[Boid::TOTAL_B+1] = {
    &Boid::seek,
    &Boid::flee,
    &Boid::walk,
    NULL
};


    Vec2d
Boid::desired_accel ()
{
    return (this->*behaviour_fn[active_behaviour])(this);
}
