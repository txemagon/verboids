#ifndef __BOID_H__
#define __BOID_H__

#include "targetable.h"
#include "vec2d.h"
#include "string"

class Boid;
typedef Vec2d (Boid::*BehaviourFn)(Boid * const);


class Boid: public Targetable
{

    public:
        /******** PUBLIC CONSTANTS ************/
        enum Behaviour {SEEK, FLEE, WALK, TOTAL_B};

    private:
        Vec2d pos, vel, tar;
        int veloz;
        std::string name_target, action;
        static const double VMAX;
        static const double AMAX;
        static BehaviourFn behaviour_fn[TOTAL_B+1];



	Targetable *target;
        Behaviour active_behaviour;

        Vec2d parse_target ();
    public:

        /********** Catalogable Methods ***********/
        Vec2d seek (Boid * const self);         /* Note the use of self instead of this */
        Vec2d flee (Boid * const self);         /* Note also the first parameter in the call is this. */
        Vec2d walk (Boid * const self);

        /********* CONSTRUCTORS *********/
	Boid (double x=0, double y=0, double vx=0, double vy=0, double tx=0, double ty=0, int nVeloz=1, std::string nName_target = "", std::string nAction = "");
	Boid (Vec2d pos, Vec2d vel = Vec2d(0,0), Vec2d tar = Vec2d(0,0), int nVeloz=1, std::string nName_target = "", std::string nAction = "");

        /******** TARGETABLE REQUIREMENTS ************/
        double x () const;
        double y () const;

        /************ GETTER & SETTERS **************/
        void set_veloz(int nVeloz);
        int get_veloz();

        void set_name_target(std::string nName_target);
        std::string get_name_target();

        void set_action(std::string nAction);
        std::string get_action();

        void set_tar(Vec2d tar);
        void set_tar(double tx, double ty);
        Vec2d get_tar();

        void set_pos ( Vec2d pos );
        void set_pos ( double x, double y );
        Vec2d get_pos ();

	void set_vel ( Vec2d vel );
        void set_vel ( double x, double y );
        Vec2d get_vel ();

	void set_target ( Targetable *target );
        Vec2d get_target ();
        void set_random_target();

        void activate (Behaviour b);
        Behaviour get_active_behaviour ();

        /*************** IA LAYER ********************/

        Vec2d desired_accel();
};

#endif
