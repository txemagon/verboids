#ifndef __MMAP_H__
#define __MMAP_H__

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

using namespace std;

class MMAP
{
    public:

        static void mmap_file(char *file, int FILE_LENGTH, int x = 2, int y = 0); // Map the file we indicate
};

#endif



