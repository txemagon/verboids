#include "freeling.h"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include <stdexcept>
#include <vector>
#include <algorithm>

#include "../interface/debug.h"

#define ST_LISTEN   0x0A
#define PORTS_FILE "/proc/net/tcp"


    /******************************************************
    *                       DATA                          *
    *******************************************************/

namespace Freeling
{
    /************ CONSTANTS *************/
    /* SUPPORTED LANGUAGES */
    enum TLan {es, /*en,*/ TOTAL_LANG};
    static const char * const config[2] = {"es.cfg", "en.cfg"};
    static const char * const languages[] = { "spanish", "english" };

    /* ANALYSIS SERVICES */
    struct TService {
        const char *option;
        const char *desc[2];
    };

    static const struct TService fr_outlv [] = {
        { "ident",    { "Identificación de idioma",  "Language identification" } },
        { "token",    { "Tokenizador",  "Tokenizer"                            } },
        { "splitted", { "Segmentador de oraciones", "Sentences splitter"       } },
        { "morfo",    { "Análisis morfológico", "Morphological analyzer"       } },
        { "tagged",   { "Etiquetado POS (Position Of Speech)", "POS Tagger"    } },
        { "shallow",  { "", ""                                                 } },
        { "parsed",   { "Analizador sintáctico", "Parser"                      } },
        { "dep",      { "Analizador de dependencias", "Dependencies analyzer"  } }/*,
        { "semgraph", { "Analizador en arbol semantico", "Semantic graph"      } }*/
        /* senses, coref and semgraph not documented */
    };

    /* Programmer decide which services to boot. */
    static const int active_services[] = { /*0, 1, 2, 3, 4, 5, 6,*/ 7/*, 8 */};  // Freeling hangs with multiple services booted.
    std::vector<int> ports[2];
    static std::vector<pid_t> service;
    static int needed_ports = 0;


    /* OUTPUT FORMATS */
    enum TOutput { freeling, conll, train, xml, json, naff };
    static const int fr_output_df = conll; /* json is another very good option. */
    static const char * const output_opt[] = { "freeling", "conll", "train", "xml", "json", "naf" };



    /************ SYSTEM RUNTIME VALUES *************/
    static bool freeling_present = false;
    static char ident_cfg[MAXSTR];  // Name of freeling ident config file.

    sig_atomic_t running_services = 0;


    /******************************************************
    *                   FUNCTIONS                         *
    *******************************************************/

    /* Signal Handlers */
    void service_down (int signum)
    {
        int status;
        --running_services;
        wait3 (&status, WNOHANG, NULL);
    }


    /* Private */

    /**
     * Checks for language identification file on the system.
     * GLOBAL VARIABLES:
     *      ident_cfg:char []       - Path to the ident file.
     */
    void find_ident ()
    {
        FILE *pipe = popen ("locate " IDENT_FILE, "r");
        fscanf ( pipe, " %[^\n]", ident_cfg );
        if ( !strlen (ident_cfg))
            throw std::invalid_argument (IDENT_FILE " not found.");
        pclose (pipe);

        LOG ( "Identification config file found at: %s\n", ident_cfg );

    }

    /**
     * Checks available ports
     * GLOBAL VARIABLES:
     *      ports: vector<int>      - Free ports for the services.
     *      needed_ports: int       - Number of ports the programmer decided to boot a service on.
     */
    void check_ports ()
    {
        char line[MAXSTR];
        int port, status;
        std::vector<int> used_ports;
        needed_ports = sizeof (active_services) / sizeof (int);


        /* Search for used ports */
        FILE *pf = fopen (PORTS_FILE, "r");

        fgets ( line, MAXSTR, pf);
        while (!feof (pf))
        {
            fscanf (pf, " %*i: %*i:%i %*i:%*i %i", &port, &status);
            fgets (line, MAXSTR, pf);
            if (status == ST_LISTEN )
                used_ports.push_back (port);
        }
        fclose (pf);

        int i = 0;
        /* Annotate available ports */
        for (int lang=es; lang<TOTAL_LANG; lang++) {
            LOG ("Looking for free ports for language %s\n", languages[lang]);
            for (int ass=0; ass<needed_ports; i++ ){
                bool found = false;
                for (int &p: used_ports)
                    if ((unsigned) p == base_port + i)
                        found = true;

                if ( !found ) {
                    LOG ("Found free port %i.\n", base_port + i);
                    ports[lang].push_back (base_port + i);
                    ass ++;
                }
            }
        }
    }

    /**
     * Boot the corresponding active_service on ports checked available.
     * GLOBAL VARIABLES:
     *      service: vector <pid_t>         - Annotate new booted service pid.
     *      running_services: sig_atomic_t  - Increment number of running services.
     */
    void boot_services ()
    {

        /* Templates for booting services */
        char * const envp[] = {
            "FREELING=" FR_PREFIX,
            "FREELINGSHARE=" FR_SHAREDIR,
            "LD_LIBRARY_PATH=" FR_LIBDIR,
           NULL
        };

        char * ar[] = {
            "/usr/local/bin/analyzer",
            "-f",
            FR_CONFDIR "es.cfg",
            "--server",
            "--port",
            "5515",
            "--outlv",
            "tagged",
            "--output",
            "conll",
            NULL,
            NULL,
            NULL
        };

        for (int l=es; l<TOTAL_LANG; l++)
         for (int i=0; i< needed_ports; i++) {

             /* Prepare command line args to boot service */
             char cur_port[MAXSTR];
             char outlv_opt[MAXSTR];
             char output[MAXSTR];
             char iopt[MAXSTR];
             char iopt_val[MAXSTR];


             /* Place values */
             sprintf (cur_port, "%i", ports[l][i] );
             strncpy (outlv_opt, fr_outlv[active_services[i]].option, MAXSTR);
             sprintf (output, "%s",  output_opt[fr_output_df]);

             ar[5] = cur_port;
             ar[7] = outlv_opt;
             ar[9] = output;


             /* Identifying language needs an extra parameter */
             // if (i == 0) {
             //     strncpy (iopt, "-I ", MAXSTR);
             //     strncpy (iopt_val, ident_cfg, MAXSTR);
             //     ar[8] = iopt;
             //     ar[9] = iopt_val;
             // }



             /* Launch service in a child process */
             pid_t child_pid = fork ();
             /* Child process */
             if (!child_pid) {


                 /* Service Launching */
                 // sleep (2);   /* Give preference to the parent */

                 /* Avoid the service using input and output */
                 int fd = open ("/dev/null", O_WRONLY | O_CREAT, 0666);
                 close (STDIN_FILENO);
                 close (STDOUT_FILENO);
                 close (STDERR_FILENO);
                 dup2 (fd, STDOUT_FILENO);
                 dup2 (fd, STDERR_FILENO);


                 execvpe ( ar[0], ar, envp);
                 /* Error will not be logged till we change messaging system */
                 // ERROR ("Error launching service %s\n", fr_outlv[i].desc[l]);
                 abort ();
             }else {
                 /* Parent process*/
                 service.push_back (child_pid);
                 running_services ++;
                 LOG ("Launching: %s on port %i\n",
                         fr_outlv[active_services[i]].desc[l], ports[l][i]);
             }
         }

    }


    /* Public */

    /**
     * Missions:
     *    - Find ident file
     *    - Check port availability.
     *    - Launch a service in a fork;
     */
    void initialize()
    {
        time_t lt;
        lt = time (NULL);
        LOG (">>>>> %s >>>>> LAUNCHING FREELING\n", asctime(localtime(&lt)));
        find_ident ();
        freeling_present = true;
        check_ports ();
        boot_services ();

        struct sigaction sa;
        bzero (&sa, sizeof (sa));
        sa.sa_handler = &service_down;
        sigaction (SIGCHLD, &sa, NULL);
    }

    void finalize()
    {
        pid_t service_pid;

        while (!service.empty ()){
            service_pid = service.back();
            kill (service_pid, SIGTERM);
            service.pop_back();
        }
        /* todo: if time passes better cut this while */
        while (running_services);
    }
}
