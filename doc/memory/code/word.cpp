#include "word.h"

/* Constructors */
Word::Word (int _id, string _name, string _pos, int _idParent, string _syntax, string _complement)
{
    id = _id;
    name = _name;
    pos = _pos;
    idParent = _idParent;
    syntax = _syntax;
    complement = _complement;
}


/* Methods */
void
Word::process(string sentence)
{

}


/* Getters */
int
Word::getId() { return id; }

string
Word::getName() { return name; }

string
Word::getPos() { return pos; }

int
Word::getIdParent() { return idParent; }

string
Word::getSyntax() { return syntax; }

string
Word::getComplement() { return complement; }


/* Setters */
void
Word::setId (int _id) { id = _id; }

void
Word::setName (string _name) { name = _name; }

void
Word::setPos (string _pos) { pos = _pos; }

void
Word::setIdParent (int _idParent) { idParent = _idParent; }

void
Word::setSyntax (string _syntax) { syntax = _syntax; }

void
Word::setComplement (string _complement) { complement = _complement; }

