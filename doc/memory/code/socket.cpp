#include "socket.h"

void socket_CS::error(const std::string &msg, int code) const {
  perror(msg.c_str());
  exit(code);
}


socket_CS::socket_CS(int port, int queue_size) {
  struct sockaddr_in server;
  startup_socket();
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) error("ERROR opening socket", sock);
  int len=sizeof(server);
  bzero((char *) &server, len);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);

  int n=bind(sock, (struct sockaddr *) &server,len);
  if (n < 0) error("ERROR on binding",n);

  listen(sock,queue_size);
}



socket_CS::socket_CS(const std::string &host, int port) {

  struct sockaddr_in server;
  startup_socket();
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) error("ERROR opening socket",sock);

  struct hostent *hp = gethostbyname(host.c_str());
  if (hp == NULL) error("Unknown host",0);

  bzero((char *) &server, sizeof(server));
  server.sin_family = AF_INET;
  bcopy((char *)hp->h_addr,(char *)&server.sin_addr.s_addr,hp->h_length);
  server.sin_port = htons(port);

  int n=connect(sock,(struct sockaddr *) &server,sizeof(server));
  if (n < 0) error("ERROR connecting",n);

  sock2=sock;
}


socket_CS::~socket_CS() {
  cleanup_socket();
}

void socket_CS::wait_client() {
  struct sockaddr_in client;
  socklen_t len = sizeof(client);
  sock2 = accept(sock,(struct sockaddr *) &client, &len);
  if (sock2 < 0) error("ERROR on accept",sock2);
}

void socket_CS::set_child() {
  int n;
  n = close_socket(sock);
  if (n < 0) error("ERROR closing socket",n);
}

void socket_CS::set_parent() {
  int n;
  n = close_socket(sock2);
  if (n < 0) error("ERROR closing socket",n);
}


int socket_CS::read_message(std::string &s) {

  int n,nt;
  char buffer[BUFF_SZ+1];

  s.clear();
  n = read_from_socket(sock2,buffer,BUFF_SZ);
  if (n < 0) error("ERROR reading from socket",n);
  buffer[n]=0;
  s = s + std::string(buffer);
  nt = n;
  while (n>0 and buffer[n-1]!=0) {
    n = read_from_socket(sock2,buffer,BUFF_SZ);
    if (n < 0) error("ERROR reading from socket",n);
    buffer[n]=0;
    //MOVE(10,0)
    s=s + std::string(buffer);
    nt += n;
  }

  return nt;
}

void socket_CS::write_message(const std::string &s) {
  int n;
  n = write_to_socket(sock2,s.c_str(),s.length()+1);
  if (n < 0) error("ERROR writing to socket",n);
}


void socket_CS::close_connection() {
  int n;
  n=close_socket(sock2);
  if (n < 0) error("ERROR closing socket",n);
}

