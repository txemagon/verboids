#include "load.h"

using namespace std;

/* Input parameters:
 * ------------------
 *  - A const argument (pointer to the position of the first letter of the argument)
 *
 * Function:
 * ------------------
 *  - Display the loading process bar
 *  - Create a loop to increase the loading bar until it reaches 100%
*/
void *
LOAD::progress_bar (void *arg)
{
    int i = 0;

    MOVE (INIT_X,INIT_Y)
    cout << "╔═══════════════╗" << endl;
    MOVE (INIT_X+1,INIT_Y)
    cout << "║               ╚╗" << endl;
    MOVE (INIT_X+2,INIT_Y)
    cout << "║               ╔╝" << endl;
    MOVE (INIT_X+3,INIT_Y)
    cout << "╚═══════════════╝" << endl;

    ANSI_SWITCH_COLOR(AC_GREEN, FORE_NORMAL) // Change the ANSI color to Green

    for(int progress = 0; progress < TOTAL_PROGRESS; progress++){

        if(progress % 7 == 0 ){
            MOVE (INIT_X_BLOCK1, INIT_Y_BLOCK + i)
            cout << "█";
            MOVE (INIT_X_BLOCK2, INIT_Y_BLOCK + i)
            cout << "█";
            i++;
        }

        usleep(DELAY);

        MOVE (X_PROGRESS, Y_PROGRESS)

        if(progress < TOTAL_PROGRESS)
            cout << progress << " %";
        cout.flush();
    }
    ANSI_RESET;   // Reset the ANSI color
}

/* Input parameters:
 * ------------------
 *  - A const argument (pointer to the position of the first letter of the argument)
 *
 * Function:
 * ------------------
 *  - Play the background song in the app
*/
void *
LOAD::music_play (void *arg)
{
  system("play artwork/HER-Load.mp3 -q");
}
