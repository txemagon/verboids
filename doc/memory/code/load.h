#ifndef __LOAD_H__
#define __LOAD_H__

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include "ansi.h"
#include "../vec2d.h"
#include "../boid.h"
#include "../mmap.h"

#define INIT_Y 84
#define INIT_X 45
#define INIT_X_BLOCK1 46
#define INIT_X_BLOCK2 47
#define INIT_Y_BLOCK 85
#define TOTAL_PROGRESS 100
#define X_PROGRESS 49
#define Y_PROGRESS 91
#define DELAY 900000
#define DIR_MUSIC "artwork/"
#define SONG "HER-Load.mp3"
#define OPTION_PLAY_MUSIC " -q"
#define PLAY_MUSIC_CONF DIR_MUSIC SONG OPTION_PLAY_MUSIC
#define PLAY_MUSIC system(PLAY_MUSIC_CONF);

using namespace std;

class LOAD
{

    public:

        static void *progress_bar (void *arg);  // Display the loading process bar
        static void *music_play (void *arg);    // Play the background song in the app
};

#endif
