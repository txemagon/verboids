#include "analyze_sentence.h"
#include "word.h"

/* Input parameters:
 * ------------------
 *  - string (text to analyze)
 *
 * Function:
 * ------------------
 *  - Divide the sentence into words and save them in a vector
 *
 * Return:
 * ------------------
 *  - Returns vector words
 */
    std::vector<std::string>
AnalyzeSentence::analyze_command ( std::string text )
{
    text += " ";

    string word;
    vector<string> command_analyze;

    for(int i = 0; i < text.length(); i++){
        if(text[i] != ' ')
            word += text[i];
        else{
            command_analyze.push_back(word);
            word = "";
        }
    }

    return command_analyze;
}

/* Input parameters:
 * ------------------
 *  - vector <Word> (list of "Word" type objects)
 *
 * Function:
 * ------------------
 *  - Analyze the sentence to get to whom it is addressed
 *
 * Return:
 * ------------------
 *  - Returns the trigger (word)
 */
    string
AnalyzeSentence::getTrigger(std::vector<Word> sentence)
{
    bool end = false;
    std::string trigger;

    for(int i = 0; (i < sentence.size() - 1) && !end; i++){
        trigger = sentence[i].getSyntax();
        if(trigger == "subj"){
            trigger = sentence[i].getName();
            end = true;
        }
    }

    return trigger;

}

/* Input parameters:
 * ------------------
 *  - vector <Word> (list of "Word" type objects)
 *
 * Function:
 * ------------------
 *  - Analyze the sentence to get what is the action
 *
 * Return:
 * ------------------
 *  - Returns the action (word)
 */
    string
AnalyzeSentence::getAction(std::vector<Word> sentence)
{
    bool end = false;
    std::string action;
    int id;

    for(int i = 0; (i < sentence.size() - 1) && !end; i++){
        action = sentence[i].getPos();
        if(action == "verb"){
            action = sentence[i].getName();
            end = true;
        }
    }

    return action;

}

/* Input parameters:
 * ------------------
 *  - vector <Word> (list of "Word" type objects)
 *
 * Function:
 * ------------------
 *  - Analyze the sentence to get what is the target
 *
 * Return:
 * ------------------
 *  - Returns the target (word)
 */
    string
AnalyzeSentence::getTarget(std::vector<Word> sentence)
{
    bool end = false;
    std::string target;
    int idParent, id;

    for(int i = 0; (i < sentence.size() - 1) && !end; i++){
        target = sentence[i].getSyntax();
        if(target == "adjt"){
            idParent = sentence[i].getId();
            for(int i = 0; (i < sentence.size() - 1) && !end; i++){
                id = sentence[i].getIdParent();
                if( id == idParent){
                    target = sentence[i].getName();
                    end = true;
                }
            }
            end = true;
        }
    }

    return target;

}

/* Input parameters:
 * ------------------
 *  - string (sentence to analyze)
 *
 * Function:
 * ------------------
 *  - Analyze the sentence obtaining all the parameters of each word and saving it in a vector of type "Word" (objects)
 *  - Display the Freeling analysis and the results of the analysis with its trigger, action and objective
 */
    void
AnalyzeSentence::analyze(std::string text)
{
    std::vector<Word> sentence;
    int cont_letter = 0,
        cont_word   = 0,
        word        = 0,
        startWord   = 0,
        endWord     = 0,
        contRead = 0;
    bool read = false;

    char * letter = new char [text.length()+1];
    strcpy (letter, text.c_str());

    sentence.push_back(Word());

    /**/    std::cout << text << std::endl;
    for(int i = 0; i < text.length(); i++){
        if (letter[i] == '\n'){
            if(cont_letter != text.length()+1){
                sentence.push_back(Word());
                word++;
                cont_word = 0;
                cont_letter++;
                contRead = 0;
            }
        }

        else if (letter[i] == ' ') {
            if(letter[i] == ' ' && letter[i+1] != ' ')
                cont_word++;
            cont_letter++;
            startWord = 0;
            read = false;
        }
        else {
            switch(cont_word){
                case 0:
                    if(sentence[word].getId() != 0)
                        sentence[word].setId( (TENUNIT + ( (int) letter[i]) + sentence[word].getId()) - ASCIINUMBER );
                    sentence[word].setId( ( (int) letter[i]) - ASCIINUMBER );
                    cont_letter++;
                    break;

                case 2:
                    if(startWord == 0)
                        startWord = cont_letter;
                    endWord = (cont_letter - startWord) + 1;
                    sentence[word].setName(text.substr(startWord, endWord));
                    cont_letter++;
                    break;

                case 5:
                    if(letter[i] == '|')
                        read = false;
                    else{
                        if(read){
                            if(startWord == 0)
                                startWord = cont_letter;
                            endWord = (cont_letter - startWord) + 1;
                            sentence[word].setPos( text.substr(startWord, endWord) );
                        }
                        if(letter[i] == '=' && contRead < 1){
                            read = true;
                            contRead++;
                        }
                    }
                    cont_letter++;
                    break;

                case 9:
                    if(sentence[word].getIdParent() != 0)
                        sentence[word].setIdParent( (TENUNIT + ( (int) letter[i]) + sentence[word].getId()) - ASCIINUMBER );
                    sentence[word].setIdParent( ( (int) letter[i]) - ASCIINUMBER );
                    cont_letter++;
                    break;

                case 10:
                    if(startWord == 0)
                        startWord = cont_letter;
                    endWord = (cont_letter - startWord) + 1;
                    sentence[word].setSyntax( text.substr(startWord, endWord) );
                    cont_letter++;
                    break;

                default:
                    cont_letter++;
            }
        }
    }

    std::string trigger = getTrigger(sentence);
    std::string action = getAction(sentence);
    std::string target = getTarget(sentence);

    if(trigger == "NULL") {trigger = "Samantha";}

    std::cout << "La frase va dirigida a: " << trigger << std::endl;
    std::cout << "La accion de la frase es: " << action << std::endl;
    std::cout << "El objetivo de la accion es: " << target << std::endl;

}
