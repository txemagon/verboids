#include "hi.h"

using namespace std;

/* Input parameters:
 * ------------------
 *  - A const text (pointer to the position of the first letter of the text)
 *
 * Function:
 * ------------------
 *  - Ask the coordinates of the object, save the first valie in X ant the second value in Y
 *  - Create a Vec2d object with the values of the X and Y
 *
 * Return:
 * ------------------
 *  - Returns the Vec2d object
 */
    Vec2d
HI::ask_coord (const char *label1, const char *label2)
{
    double x, y;

    system ("clear");
    cout << label1;
    cin >> x;
    cout << label2;
    cin >> y;

    Vec2d data(x, y);

    return data;
}

/* Input parameters:
 * ------------------
 *  - A const text (pointer to the position of the first letter of the text)
 *  - A Vec2d object
 *
 * Function:
 * ------------------
 *  - Show the X and Y values of the objects
 */
     void
HI::show_vector (const char *label, Vec2d v)
{
    cout << endl << "-> " << label << ": [" << v.x () <<"," << v.y() <<"]" << endl;
}

    string
HI::show_vector (Vec2d v)
{
    string pos;
    pos = "[" + std::to_string((int)v.x()) + "," + std::to_string((int)v.y()) + "]";
    return pos;
}

/* Function:
 * ------------------
 *  - Print the program homepage
 *  - Show the program logo by mapping the "samantha.scr" file with mmap
 */
    void
HI::welcome(char *file, int FILE_LENGTH, bool stop, int x, int y)
{
    if(stop) {
        MMAP::mmap_file(file, FILE_LENGTH, x, y);
        cin.ignore();
    }
    else
        MMAP::mmap_file(file, FILE_LENGTH, x, y);
}

/* Function:
 * ------------------
 *  - Initialize the program by cleaning the screen and move
 *    the prompt to the indicated position.
 */
    void
HI::init ()
{
    system ("clear");
    welcome( (char *) "artwork/title.scr", 1500, false);
    MOVE_TO(10,0)
}

/* Function:
 * ------------------
 *  - Restore colors
 */
    void
HI::finalize ()
{
    ANSI (ANSI_RESET);
}

/**
 *  Clean the canvas to show new data.
 */
void
HI::clean_canvas (){

    MOVE_TO(10, 0 )   // In this position the canvas begins

    // Clean the canvas line by line to position 50
    for(int i = 0; i < 50; i++) {
        cout << "                                                                                                                                                                                                          ";
        MOVE(9 + i,170)
        cout << "                                       ";
        MOVE (10 + i, 0)
    }

    // Return the pointer to the beginning of the canvas
    MOVE_TO(10,0)
}

/**
 *   Create a canvas that will start on line 10
 *   and end on line 49 where the data will be displayed
 */
void
HI::canvas (map<string, Boid> boidObjects) {

    clean_canvas();

    MOVE(9,170)
    cout << "Name\t" << "Vel\t" << "Pos\t" << " Tar" << endl;

    map<string, Boid>::iterator it = boidObjects.begin();

    for(int i = 0; it != boidObjects.end(); it++, i++){
        MOVE(10 + i, 170)
            if (it->second.get_pos().y() < 100)
                cout << it->first << "\t" << it->second.get_veloz() << "\t" << show_vector(it->second.get_pos()) << "\t" << " " << show_vector(it->second.get_tar()) << endl;
            else
                cout << it->first << "\t" << it->second.get_veloz()  << "\t" << show_vector(it->second.get_pos()) << " " << show_vector(it->second.get_tar()) << endl;
        MOVE(10 + (int) it->second.get_pos().x(), i + (int) it->second.get_pos().y() )
            cout << "●" << endl;
    }

    // Return the pointer to the beginning of the canvas
    MOVE_TO(10,0)
}

void
HI::canvas_help () {

    clean_canvas();
    cout << "\t\t\t\t\t\tVERBOIDS USER MANUAL" << endl;
    cout << "\t\t\t\t\t\t-------------------" << endl;
    cout << "\n\t\thelp - Display a help manual for using the program with the commands that can be used." << endl;
    cout << "\n\n\t\tActivate Samantha - Activate Samantha (text parser) and disable AI boids." << endl;
    cout << "\t\t\tOther similar commands - activate samantha | Activate samantha" << endl;
    cout << "\t\tDesactivate Samantha - Disable Samantha (text parser) and activate AI boids." << endl;
    cout << "\t\t\tOther similar commands - desactivate samantha | Desactivate samantha" << endl;
    cout << "\n\n\t\tDelete [Boid name] - Delete the boid you indicate." << endl;
    cout << "\t\t\tOther similar commands - Remove [Boid name]" << endl;
    cout << "\t\tCreate [Boid name] - Create new Boid." << endl;
    cout << "\n\n\t\t[Boid name] new speed [new speed]- Set new speed." << endl;
    cout << "\t\t\tOther similar commands - [Boid name] speed [new speed]." << endl;
    cout << "\t\t[Boid name] seek [Target boid name] - Display a help manual for using the program with the commands that can be used." << endl;
    cout << "\t\t\tOther similar commands - [Boid name] chase [Target boid name] || [Boid name] follow [Target boid name]." << endl;
    cout << "\t\t[Boid name] free - Activate walk behavior and removes its target." << endl;
    MOVE(48,0)
    cout << "\tQUIT (ENTER)" << endl;
    prompt_command();

}

/**
 *   Create a prompt to write instructions and return that instruction
 *
 * RETURN VALUE:
 *  - Returns the instruction
 */
    string
HI::prompt_command ()
{
    MOVE_TO(50,0)
        cout << "                                                                             \r"; // Clean the prompt

    ANSI_SWITCH_COLOR(AC_GREEN, FORE_NORMAL);                 // Change the color of the prompt to green

    char *buf = readline ("OS-1 : Samantha > " ANSI_RESET);   // Create a prompt and save the instructions we write in buf, reset color ANSI

    UP(1)

    // If the user has not written anything is ignored
    if ( buf == nullptr)
        throw;

    // Save the instruction in the history
    if ( strlen (buf) > 0)
        add_history (buf);

    string command (buf);   // Convert char array to string (is the same of "string command = buf")

    free (buf);             // Free the buf from memory

    return command;
}
