#ifndef __ANALYZE_SENTENCE_H__
#define __ANALYZE_SENTENCE_H__

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>

#include "word.h"

#define FILENAME "analyze.txt"
#define ASCIINUMBER 48
#define TENUNIT 10

using namespace std;

class AnalyzeSentence
{

    public:

        static std::vector<std::string> analyze_command ( std::string text );   // Divide the sentence into words and save them in a vector
        static void analyze(string text);                                       // Analyze the sentence obtaining all the parameters of each word
        static string getTrigger(vector<Word> sentence);                        // Analyze the sentence to get to whom it is addressed
        static string getAction(vector<Word> sentence);                         // Analyze the sentence to get what is the action
        static string getTarget(vector<Word> sentence);                         // Analyze the sentence to get what is the target

};


#endif
