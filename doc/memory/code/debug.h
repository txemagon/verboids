#ifndef __DEBUG_H__
#define __DEBUG_H__

#define LVLNON 0
#define LVLERR 1
#define LVLLOG 2
#define LVLWAR 3
#define LVLDBG 4

#define DEBUG_LEVEL LVLLOG

#if DEBUG_LEVEL == LVLNON
#define ERROR(...)
#define LOG(...)
#define WARN(...)
#define DEBUG(...)
#endif

#if DEBUG_LEVEL > LVLNON
#define ERROR(...) fprintf (stderr, __VA_ARGS__ );
#endif

#if DEBUG_LEVEL > LVLERR
#define LOG(...) fprintf (stderr, __VA_ARGS__ );
#endif


#if DEBUG_LEVEL > LVLLOG
#define WARN(...) fprintf (stderr, __VA_ARGS__ );
#endif


#if DEBUG_LEVEL > LVLWARN
#define DEBUG(...) fprintf (stderr, __VA_ARGS__ );
#endif


#endif
