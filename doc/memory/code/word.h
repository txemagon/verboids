#ifndef __WORD_H__
#define __WORD_H__

#include <iostream>
#include <string.h>

#define ASCIINUMBER 48
#define TENUNIT 10

using namespace std;

class Word
{
    private:

        int id;
        string name;
        string pos;
        int idParent;
        string syntax;
        string complement;

    public:

        /* Constructors */
        Word (int _id = 0, string _name = "NULL", string _pos = "NULL", int _idParent = 0, string _syntax = "NULL", string _complement = "NULL");


        /* Methods */
        void process(string sentence);


        /* Getters */
        int getId();
        string getName();
        string getPos();
        int getIdParent();
        string getSyntax();
        string getComplement();


        /* Setters */
        void setId (int _id);
        void setName (string _name);
        void setPos (string _pos);
        void setIdParent (int _idParent);
        void setSyntax (string _syntax);
        void setComplement (string _complement);



};

#endif
