#ifndef _SOCKET_CS
#define _SOCKET_CS


#include <string>
#include "ansi.h"
#if defined WIN32 || defined WIN64
  #include <winsock2.h>
  #include "iso646.h"
  #define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
  #define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)
  #define read_from_socket(sck,bf,sz) recv(sck,bf,sz,NULL)
  #define write_to_socket(sck,str,sz) send(sck,str,sz,NULL)
  #define close_socket(sck) closesocket(sck)
  #define startup_socket() WSADATA wsaData; \
                    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData); \
                    if (iResult != NO_ERROR) \
                       error("Error at WSAStartup()\n", iResult);
  #define cleanup_socket() WSACleanup()
  #define socklen_t int
#else
  #include <string.h>
  #include <cstdlib>
  #include <cstdio>
  #include <unistd.h>
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #include <netdb.h>
  #define read_from_socket(sck,bf,sz) read(sck,bf,sz)
  #define write_to_socket(sck,str,sz) write(sck,str,sz)
  #define close_socket(sck) close(sck);
  #define startup_socket()
  #define cleanup_socket()
  #define SOCKET int
#endif

#define SOCK_QUEUE_SZ 5
#define BUFF_SZ 2048

class socket_CS {
  private:
  SOCKET sock, sock2;
  void error(const std::string &,int) const;

  public:
    socket_CS(int port, int qsize=SOCK_QUEUE_SZ);
    socket_CS(const std::string&, int);

    ~socket_CS();

    void wait_client();
    int read_message(std::string&);
    void write_message(const std::string &);
    void close_connection();
    void set_child();
    void set_parent();
};

#endif

