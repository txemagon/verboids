#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>
#include "string"

#include "../vec2d.h"
#include "../boid.h"

#include "ai.h"
#include "ansi.h"
#include "debug.h"
#include "../freeling/freeling.h"
#include "word.h"
#include "analyze_sentence.h"

socket_CS *AI::sock = NULL;

/* Input parameters:
 * ------------------
 *  - pointer arg (pointer to the position of the first letter of the argument)
 *
 * Function:
 * ------------------
 *  - Create a socket connected with Freeling to write and read texts that Freeling parses
*/
void *
AI::init (void *arg)
{
    std::string host = "localhost";
    std::string r;

    sleep (SLEEP);  // Esperar hasta que el servicio esté arrancado.
    sock = new socket_CS(host, Freeling::ports[0][0]);
    sock->write_message ("RESET STATS"); // sync message
    sock->read_message (r);
    if (r!="FL-SERVER-READY") {
        std::cerr<<"Server not ready? Its answer was: [" << r << "]" << std::endl;
        exit(1);
    }
    sock->write_message("FLUSH_BUFFER");
    sock->read_message(r);
}

/* Function:
 * ------------------
 *  - Properly close the connection to the socket
*/
void
AI::finalize ()
{
    std::string r;
    sock->write_message("FLUSH_BUFFER");
    sock->read_message(r);

    if (r!="FL-SERVER-READY")
        std::cout<<r; // output response, if any

    sock->close_connection ();
    delete sock;

}

/* Input parameters:
 * ------------------
 *  - string command (text to parse)
 *
 * Function:
 * ------------------
 *  - Send command to socket to parse
 *  - Send the Freeling analysis to analyze and adapt it
*/
void
AI::process( std::string command )
{
    std::string r;

    sock->write_message(command);
    sock->read_message(r);

    sock->write_message("FLUSH_BUFFER");
    sock->read_message(r);

    if (r!="FL-SERVER-READY")
        AnalyzeSentence::analyze(r);
}

/* Input parameters:
 * ------------------
 *  - map &boidObjects (list of boids by reference)
 *  - int &vuelta (number of while repetitions per reference to know if the boid has to perform its behavior)
 *  - string command (text)
 *
 * Function:
 * ------------------
 *  - Develops the logic of boid behavior
 *  - If a command exists, it is parsed to develop its function
*/
void
AI::logic ( map<string, Boid> &boidObjects, int &vuelta, std::string command )
{
    map<string, Boid>::iterator it = boidObjects.begin();                                 // Map list of boids
    vector<std::string> command_analyze = AnalyzeSentence::analyze_command ( command );   // Save command analysis

    /**
     * Loop to develop the behavior of each boid
    */
    for(; it != boidObjects.end(); it++){
        double x = it->second.get_pos().x(),    // Save X position of the boid
               y = it->second.get_pos().y();    // Save Y position of the boid

        /**
         * - Check if the speed of the boid is greater than or equal to the lap number
         * - If the speed is higher, analyze the next position of the boid
         * - If you have reached your target, you are randomly assigned a new target
        */
        if( it->second.get_veloz() >= vuelta ) {
            // Analyze the next position
            if( it->second.get_pos().x() == it->second.get_tar().x() ) { x = it->second.get_pos().x(); }
            else if(it->second.get_pos().x() > it->second.get_tar().x()) { x = it->second.get_pos().x() - 1; }
            else { x = it->second.get_pos().x() + 1; }

            if ( it->second.get_pos().y() == it->second.get_tar().y() ) { y = it->second.get_pos().y(); }
            else if(it->second.get_pos().y() > it->second.get_tar().y()) { y = it->second.get_pos().y() - 1; }
            else { y = it->second.get_pos().y() + 1; }

            // Set the new position
            it->second.set_pos(x, y);

            // Set a new target, if the boid have reached
            if (it->second.get_pos().x() == it->second.get_tar().x() && it->second.get_pos().y() == it->second.get_tar().y())
                it->second.set_random_target();
        }

        /**
         * - If the first word of the command is Delete or Remove, remove the indicated boid
         * - Example: Delete Boid1
        */
        if(command_analyze[0] == "Delete" || command_analyze[0] == "Remove")
            boidObjects.erase(command_analyze[1]);

        /**
         * - If the first word of the command is Create, create a new boid with the indicated name
         * - Example: Create Boid20
        */
        if(command_analyze[0] == "Create")
            boidObjects.insert(make_pair<string, Boid>( (std::string) command_analyze[1], Boid(1,1,2,3,3,2,3) ));

        /**
         * - If the first word of the command is a boid name
         * - Example: Boid1 seek Boid2 || Boid1 speed 5
        */
        if( command_analyze[0] == it->first) {
            /**
             * - Set new speed if the second and third word are "new speed"
             * - Example: Manolito new speed 3 || Boid2 new speed 4
            */
            if( command_analyze[1] == "new" && command_analyze[2] == "speed"){
                int nVeloz = std::stoi(command_analyze[3]);
                it->second.set_veloz( nVeloz );
            }
            /**
             * - Set new speed if the second word is "speed"
             * - Example: Juanito speed 3 || Boid15 new speed 4
            */
            else if( command_analyze[1] == "speed") {
                int nVeloz = std::stoi(command_analyze[2]);
                it->second.set_veloz( nVeloz );
            }
            /**
             * - Set behavior free
             * - Example: Boid1 free
            */
            else if ( command_analyze[1] == "free"){
                it->second.set_name_target("");
                it->second.set_random_target();
            }
            /**
             * - Set the name of the boid that is your new target
             * - Set new behavior
             * - Example: Boid10 seek Boid2 || Juanito follow Manolito
            */
            else {
                it->second.set_name_target( (std::string) command_analyze[2]);
                it->second.set_action(command_analyze[1]);
            }
        }

        /**
         * - Develop boid behavior
        */
        if( it->second.get_name_target() != "" ) {
            /*
             * - Seek behavior - get the position of your target and update the position the boid has to go to
            */
            if (it->second.get_action() == "follow" || it->second.get_action() == "chase" || it->second.get_action() == "seek") {
                std::string target = it->second.get_name_target();
                it->second.set_tar( boidObjects.find(target)->second.get_pos().x(), boidObjects.find(target)->second.get_pos().y() );
            }
        }
    }
}
