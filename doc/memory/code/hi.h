#ifndef __HI_H__
#define __HI_H__

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <readline/readline.h>
#include <readline/history.h>
#include <map>

#include "ansi.h"
#include "../vec2d.h"
#include "../boid.h"
#include "../mmap.h"

#define MOVE_TO(x,y)        cout << "\x1B[" #x ";" #y "f";            // Move the promt pointer to the indicated position
#define MOVE_TO_CLEAN(x)    cout << "\x1B[" #x;                       // Move the promt pointer to the indicated position
#define UP(x)               cout << "\x1B[" #x "A";                   //
#define TITLE               system("toilet -f mono9 '   VERBOIDS' "); // Title of the program

using namespace std;

class HI
{

    public:
        enum Options {Menu, Salir};

   /* Interfaz antiguo */
   static Vec2d ask_coord (const char *label1, const char *label2); // Ask the coordinates of the object
   static void clean_canvas();                             // Clean the canvas to print new data
   static void canvas (map<string, Boid> boidObjects);     // Canvas to show the menus and exits of the program
   static void canvas_help ();     // Canvas to show the menus and exits of the program
   static void show_vector (const char *label, Vec2d v);  // Show the X and Y values of the objects
   static string show_vector (Vec2d v);                     // Show the X and Y values of the objects

   /* Interfaz nuevo */
   static void welcome(char *file, int FILE_LENGTH, bool stop, int x = 0, int y = 0); // Print the home screen
   static void init ();             // Initialize the program by cleaning the screen and moving the prompt to the indicated position
   static void finalize ();         // Restore colors
   static string prompt_command (); // Create a prompt to write instructions and return that instruction


};

#endif
