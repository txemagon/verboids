#ifndef __FREELING_FREELING_H__
#define __FREELING_FREELING_H__

#include <string>
#include <vector>
#include "../interface/options.h"

#define MAXSTR 0x100
#define FR_PREFIX  "/usr/local/"
#define FR_SHAREDIR FR_PREFIX "share/freeling/"
#define FR_CONFDIR FR_SHAREDIR "config/"
#define FR_LIBDIR  FR_PREFIX "lib/"
#define IDENT_FILE "ident.dat"


/* Module to handle Freeling Options */

namespace Freeling {
    extern std::vector<int> ports[2];

    extern void initialize ();
    extern void finalize ();
}

#endif

