\babel@toc {spanish}{}
\contentsline {chapter}{Índice general}{\es@scroman {iii}}{section*.2}%
\contentsline {chapter}{Índice de figuras}{\es@scroman {iv}}{section*.3}%
\contentsline {chapter}{\chapternumberline {1}Memoria}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Presentación}{1}{section.1.1}%
\contentsline {subsection}{Resumen}{1}{section*.5}%
\contentsline {subsection}{Motivación}{2}{section*.6}%
\contentsline {subsection}{Alcance}{2}{section*.7}%
\contentsline {subsection}{Objetivo}{3}{section*.8}%
\contentsline {section}{\numberline {1.2}Exposición}{7}{section.1.2}%
\contentsline {subsection}{Tecnologías y herramientas empleadas}{7}{section*.9}%
\contentsline {subsection}{Funcionamiento del programa}{8}{section*.10}%
\contentsline {subsection}{Herramienta Freeling}{9}{section*.11}%
\contentsline {subsection}{Diseño de los boids}{10}{section*.12}%
\contentsline {subsection}{Diseño de la interfaz}{12}{section*.13}%
\contentsline {subsection}{Diseño de la inteligencia artificial de los Boids}{13}{section*.14}%
\contentsline {subsection}{Diseño de la inteligencia artificial de Samantha}{15}{section*.15}%
\contentsline {subsection}{Pruebas}{17}{section*.16}%
\contentsline {section}{\numberline {1.3}Conclusiones}{19}{section.1.3}%
\contentsline {subsection}{Grado de Consecución de los Objetivos}{19}{section*.17}%
\contentsline {subsection}{Análisis del Alcance}{22}{section*.18}%
\contentsline {subsection}{Puntos de Interés}{22}{section*.19}%
\contentsline {subsection}{Aprendizajes}{23}{section*.20}%
\contentsline {subsection}{Reflexión}{23}{section*.21}%
\contentsline {chapter}{\chapternumberline {2}Anexos}{25}{chapter.2}%
\contentsline {section}{\numberline {2.1}Diagramas UML}{25}{section.2.1}%
\contentsline {subsection}{Diagrama de clases}{25}{section*.22}%
\contentsline {subsection}{Diagrama de actividad}{26}{section*.23}%
\contentsline {subsection}{Diagrama de casos de uso}{27}{section*.24}%
\contentsline {subsection}{Diagrama de secuencia}{27}{section*.25}%
\contentsline {section}{\numberline {2.2}Código}{28}{section.2.2}%
\contentsline {subsection}{Main.cpp}{28}{section*.26}%
\contentsline {subsection}{Mmap.cpp}{31}{section*.27}%
\contentsline {subsection}{Mmap.h}{32}{section*.28}%
\contentsline {subsection}{Boid.cpp}{32}{section*.29}%
\contentsline {subsection}{Boid.h}{36}{section*.30}%
\contentsline {subsection}{Vec2d.cpp}{37}{section*.31}%
\contentsline {subsection}{Vec2d.h}{40}{section*.32}%
\contentsline {subsection}{targetable.h}{41}{section*.33}%
\contentsline {subsection}{Makefile}{41}{section*.34}%
\contentsline {subsection}{Freeling}{42}{section*.35}%
\contentsline {subsection}{Freeling.h}{47}{section*.36}%
\contentsline {subsection}{Ai.cpp}{47}{section*.37}%
\contentsline {subsection}{Ai.h}{51}{section*.38}%
\contentsline {subsection}{Analyze\_sentence.cpp}{52}{section*.39}%
\contentsline {subsection}{Analyze\_sentence.h}{56}{section*.40}%
\contentsline {subsection}{Ansi.h}{57}{section*.41}%
\contentsline {subsection}{Debug.h}{58}{section*.42}%
\contentsline {subsection}{Hi.cpp}{59}{section*.43}%
\contentsline {subsection}{Hi.h}{62}{section*.44}%
\contentsline {subsection}{Interface.h}{63}{section*.45}%
\contentsline {subsection}{Load.cpp}{64}{section*.46}%
\contentsline {subsection}{Load.h}{65}{section*.47}%
\contentsline {subsection}{Options.cpp}{66}{section*.48}%
\contentsline {subsection}{Options.h}{66}{section*.49}%
\contentsline {subsection}{Socket.cpp}{66}{section*.50}%
\contentsline {subsection}{Socket.h}{68}{section*.51}%
\contentsline {subsection}{Word.cpp}{69}{section*.52}%
\contentsline {subsection}{Word.h}{70}{section*.53}%
\contentsline {subsection}{Samantha.src}{71}{section*.54}%
\contentsline {subsection}{Samantha-load.scr}{73}{section*.55}%
\contentsline {subsection}{Title.scr}{75}{section*.56}%
\contentsline {chapter}{Glosario}{77}{section*.57}%
\contentsline {chapter}{Bibliografía}{79}{chapter*.59}%
